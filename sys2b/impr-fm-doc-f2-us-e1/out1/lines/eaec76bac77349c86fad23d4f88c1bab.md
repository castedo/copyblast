Where [vegetation](Antarctic_flora) is found, it primarily consists of [lichens](lichen) or [mosses](moss).
