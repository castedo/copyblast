Despite the continent's remoteness, human activity significantly impacts it through [pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
