This is possible because Earth is a [water world](ocean_world), the only one in the [Solar System](Solar_System) capable of sustaining liquid [surface water](surface_water).
