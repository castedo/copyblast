The [Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted relative to the perpendicular of its orbital plane around the Sun, leading to the formation of seasons.
