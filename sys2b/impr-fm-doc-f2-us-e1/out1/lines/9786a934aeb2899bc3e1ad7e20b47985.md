This process maintains the current average surface temperature at 14.76&nbsp;°C, a level at which water remains liquid under atmospheric pressure.
