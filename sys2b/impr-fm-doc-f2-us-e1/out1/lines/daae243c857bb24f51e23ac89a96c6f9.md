The Moon's gravity not only stabilizes Earth's axis but also causes [tides](tide), which [gradually slow Earth's rotation](Tidal_acceleration).
