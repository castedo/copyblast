This process maintains the current average surface temperature at 14.76&nbsp;°C, a level that enables water to stay in its liquid form under atmospheric pressure.
