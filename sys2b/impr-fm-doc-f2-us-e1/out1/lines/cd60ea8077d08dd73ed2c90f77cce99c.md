As a result of [tidal locking](tidal_locking), the same side of the Moon is always facing the Earth.
