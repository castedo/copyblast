During the summer months, approximately 5,000 individuals reside at [research stations](Research_stations_in_Antarctica), a number that decreases to around 1,000 in the winter.
