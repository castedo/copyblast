Earth, similar to most other celestial bodies in the Solar System, [formed 4.5 billion years ago](age_of_Earth) from gas in the [early Solar System](formation_and_evolution_of_the_Solar_System).
