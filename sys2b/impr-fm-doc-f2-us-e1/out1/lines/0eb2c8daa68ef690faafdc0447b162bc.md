Life has thrived across the globe, significantly altering Earth's atmosphere and surface.
This led to the [Great Oxidation Event](Great_Oxidation_Event) two billion years ago.
