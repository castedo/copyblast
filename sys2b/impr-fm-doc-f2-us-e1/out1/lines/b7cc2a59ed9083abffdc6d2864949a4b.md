The Moon's gravity not only helps stabilize Earth's axis but also causes [tides](tide), which [gradually slow Earth's rotation](Tidal_acceleration).
