Antarctica is, on average, the coldest, driest, and windiest continent,
boasting the highest average [elevation](elevation).
Primarily a [polar desert](polar_desert),
it receives annual [precipitation](Climate_of_Antarctica#Precipitation) of over 200 mm (8 in) along the coast and significantly less inland.
Approximately 70% of the world's [freshwater](freshwater) reserves are locked in Antarctica,
which, if melted, would cause global [sea levels](sea_level) to rise by almost 60 metres (200 ft).
Antarctica also holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth),
at &minus;89.2 °C (&minus;128.6 °F).
During summer, coastal regions can experience temperatures above 10 °C (50 °F).
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) is found, it predominantly consists of [lichen](lichen) and [moss](moss).
