The Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and
protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is widely present in the atmosphere,
[forming clouds](Cloud#Formation) that cover most of the planet.
This water vapor acts as a [greenhouse gas](greenhouse_gas) and,
along with other greenhouse gases in the atmosphere,
especially [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
traps
[energy from the Sun's light](Solar_irradiance).
This process maintains the Earth's average surface temperature at 14.76&nbsp;°C,
a temperature at which water remains liquid under atmospheric pressure.

Variations in the amount of energy captured in different geographic regions (with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions))
drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
producing a global [climate system](climate_system) with various [climate regions](climate_region).
This system supports a range of weather phenomena, such as [precipitation](precipitation),
and allows components like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through the environment.
