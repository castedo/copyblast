Under the terms of the treaty, military operations, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal) are all prohibited in Antarctica.
