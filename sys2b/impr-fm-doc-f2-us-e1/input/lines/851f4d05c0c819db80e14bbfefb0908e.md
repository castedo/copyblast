The [Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted relative to the perpendicular of its orbital plane around the Sun, resulting in the formation of seasons.
