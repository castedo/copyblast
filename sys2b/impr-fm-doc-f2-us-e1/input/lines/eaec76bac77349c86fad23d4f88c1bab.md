Where [vegetation](Antarctic_flora) is found, it primarily consists of [lichen](lichen) or [moss](moss).
