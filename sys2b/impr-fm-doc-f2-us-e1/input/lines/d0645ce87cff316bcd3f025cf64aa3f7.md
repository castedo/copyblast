The Moon's gravity not only stabilizes the Earth's axis but also causes [tides](tide), which [gradually slow the Earth's rotation](Tidal_acceleration).
