Life has proliferated worldwide, profoundly transforming Earth's atmosphere and surface.
This resulted in the [Great Oxidation Event](Great_Oxidation_Event) two billion years ago.
