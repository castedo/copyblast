Approximately 70% of the world's [freshwater](freshwater) reserves are contained within the ice of Antarctica.
Should this ice melt, it would lead to a global [sea level](sea_level) rise of nearly 60 meters (200 feet).
