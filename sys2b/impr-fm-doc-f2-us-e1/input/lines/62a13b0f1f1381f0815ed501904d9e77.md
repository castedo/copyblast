It is primarily a [polar desert](polar_desert), receiving an annual [precipitation](Climate_of_Antarctica#Precipitation) of more than 200 mm (8 in) along the coast and significantly less inland.
