The Earth is approximately eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit), taking about 365.25 days, or one year, to complete a single revolution.
