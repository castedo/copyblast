Antarctica holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth), at &minus;89.2 °C (&minus;128.6 °F).
