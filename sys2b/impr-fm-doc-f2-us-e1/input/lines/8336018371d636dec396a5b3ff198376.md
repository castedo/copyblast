This process maintains the current average surface temperature at 14.76&nbsp;°C, a level that allows water to remain in its liquid state under atmospheric pressure.
