Earth is about eight [light-minutes](light-minute) away from the Sun and [orbits it](Earth's_orbit), taking a year (about 365.25 days) to complete one revolution.
