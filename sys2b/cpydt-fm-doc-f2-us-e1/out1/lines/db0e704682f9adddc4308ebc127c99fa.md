As a result of [tidal locking](tidal_locking), the same side of the Moon always faces the Earth.
