Where [vegetation](Antarctic_flora) occurs, it is primarily in the form of [lichen](lichen) or [moss](moss).
