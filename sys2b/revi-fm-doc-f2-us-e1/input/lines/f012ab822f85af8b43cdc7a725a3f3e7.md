Antarctica is [governed by approximately 30 countries](Territorial_claims_in_Antarctica), all of which are signatories to the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
