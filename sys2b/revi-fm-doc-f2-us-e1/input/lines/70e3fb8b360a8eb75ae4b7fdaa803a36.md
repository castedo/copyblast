The Earth's crust consists of slowly moving [tectonic plates](plate_tectonics) that interact to form mountain ranges, [volcanoes](volcano), and earthquakes.
