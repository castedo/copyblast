Where [vegetation](Antarctic_flora) occurs, it primarily takes the form of [lichen](lichen) or [moss](moss).
