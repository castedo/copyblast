Nearly all of Earth's water is contained within its vast oceans, covering [70.8%](water_distribution_on_Earth) of the [Earth's crust](Earth's_crust).
