Earth is approximately eight [light-minutes](light-minute) away from the Sun and [orbits it](Earth's_orbit), taking about 365.25 days, or a year, to complete one revolution.
