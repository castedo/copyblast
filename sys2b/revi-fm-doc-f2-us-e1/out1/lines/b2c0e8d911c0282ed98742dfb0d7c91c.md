Humans depend on Earth's [biosphere](biosphere) and natural resources for survival, yet have [increasingly impacted the planet's environment](Human_impact_on_the_environment).
