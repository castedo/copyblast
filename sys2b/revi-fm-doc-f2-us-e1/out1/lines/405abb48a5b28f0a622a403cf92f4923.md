[Water vapor](Water_vapor) is prevalent in the atmosphere, [forming clouds](Cloud#Formation) that envelop much of the planet.
