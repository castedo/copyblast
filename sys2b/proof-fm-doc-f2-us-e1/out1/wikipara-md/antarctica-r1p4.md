Antarctica is [governed by about 30 countries](Territorial_claims_in_Antarctica),
all of which are parties to the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
According to the terms of the treaty,
military activity, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal)
are all prohibited in Antarctica.
[Tourism](Tourism_in_Antarctica), fishing, and research are the main human activities in and around Antarctica.
During the summer months,
about 5,000 people reside at [research stations](Research_stations_in_Antarctica),
a figure that drops to around 1,000 in the winter.
Despite the continent's remoteness,
human activity has a significant effect on it
via [pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
