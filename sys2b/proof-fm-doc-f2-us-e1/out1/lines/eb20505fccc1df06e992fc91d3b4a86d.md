Despite the continent's remoteness, human activity significantly affects it through [pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
