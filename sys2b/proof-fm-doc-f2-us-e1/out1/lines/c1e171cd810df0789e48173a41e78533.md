[Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted with respect to the perpendicular of its orbital plane around the Sun, producing seasons.
