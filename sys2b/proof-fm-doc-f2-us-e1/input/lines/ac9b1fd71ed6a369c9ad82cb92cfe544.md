Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that sustains Earth's surface conditions and protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
