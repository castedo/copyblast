**Antarctica** is [Earth](Earth)'s southernmost and least-populated [continent](continent).
Situated almost entirely south of the [Antarctic Circle](Antarctic_Circle) and surrounded by the [Southern Ocean](Southern_Ocean) (also known as the [Antarctic Ocean](Antarctic_Ocean)), it contains the geographic [South Pole](South_Pole).
Antarctica is the fifth-largest continent, being about 40% larger than [Europe](Europe), and has an area of 14,200,000 km² (5,500,000 sq mi).
Most of Antarctica is covered by the [Antarctic ice sheet](Antarctic_ice_sheet), with an average thickness of 1.9 km (1.2 mi).

Antarctica is, on average, the coldest, driest, and windiest of the continents, and it has the highest average [elevation](elevation).
It is mainly a [polar desert](polar_desert), with annual [precipitation](Climate_of_Antarctica#Precipitation) of over 200 mm (8 in) along the coast and far less inland.
About 70% of the world's [freshwater](freshwater) reserves are frozen in Antarctica, which, if melted, would raise global [sea levels](sea_level) by almost 60 metres (200 ft).
Antarctica holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth), &minus;89.2 °C (&minus;128.6 °F).
The coastal regions can reach temperatures over 10 °C (50 °F) in the summer.
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped) and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) occurs, it is mostly in the form of [lichen](lichen) or [moss](moss).

The [ice shelves of Antarctica](Antarctic_ice_sheet) were probably first seen in 1820, during [a Russian expedition](First_Russian_Antarctic_Expedition) led by [Fabian Gottlieb von Bellingshausen](Fabian_Gottlieb_von_Bellingshausen) and [Mikhail Lazarev](Mikhail_Lazarev).
The decades that followed saw further [exploration](List_of_Antarctic_expeditions) by French, American, and British expeditions.
The first confirmed landing was by a Norwegian team in 1895.
In the early 20th century, there were a few expeditions into the interior of the continent.
[British explorers](Nimrod_Expedition) were the first to reach the [magnetic South Pole](South_magnetic_pole) in 1909, and the [geographic South Pole](Geographic_south_pole) was first reached in 1911 by [Norwegian explorers](Amundsen's_South_Pole_expedition).

Antarctica is [governed by about 30 countries](Territorial_claims_in_Antarctica), all of which are parties of the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
According to the terms of the treaty, military activity, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal) are all prohibited in Antarctica.
[Tourism](Tourism_in_Antarctica), fishing and research are the main human activities in and around Antarctica.
During the summer months, about 5,000 people reside at [research stations](Research_stations_in_Antarctica), a figure that drops to around 1,000 in the winter.
Despite the continent's remoteness, human activity has a significant effect on it via [pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
