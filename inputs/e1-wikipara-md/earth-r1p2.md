Earth has [a dynamic atmosphere](Atmosphere_of_Earth),
which sustains Earth's surface conditions and
protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
It has a composition of primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is widely present in the atmosphere,
[forming clouds](Cloud#Formation) that cover most of the planet.
The water vapor acts as a [greenhouse gas](greenhouse_gas) and,
together with other greenhouse gases in the atmosphere,
particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
creates the conditions for both liquid surface water and
water vapor to persist by capturing [energy from the Sun's light](Solar_irradiance).
This process maintains the current average surface temperature of 14.76&nbsp;°C,
at which water is liquid under atmospheric pressure.
Differences in the amount of captured energy between geographic regions
(as with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions))
drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
producing a global [climate system](climate_system) with different [climate regions](climate_region),
and a range of weather phenomena such as [precipitation](precipitation),
allowing components such as [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle).
