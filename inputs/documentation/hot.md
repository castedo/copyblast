Hot Copyediting
===============

Copy**AI**d is installed with a single example request settings file: `cold-example.toml`.
On most Linux distributions this will be installed at `~/.config/copyaid/cold-example.toml`.


Cold Requests
-------------

This request is "cold" because it has the following settings:

```
[openai]
n = 1
temperature = 0
```

The `n = 1` setting instructs OpenAI to return only one single revision.
The `temperature = 0` instructs OpenAI to focus on returning its single best choice for
the request.

Here is the description of this settings from the
[OpenAI API reference](https://platform.openai.com/docs/api-reference/chat/create):

> What sampling temperature to use, between 0 and 2.
> Higher values like 0.8 will make the output more random,
> while lower values like 0.2 will make it more focused and deterministic.


Hot Requests
------------

In contrast, hot requests have a settings of `temperature` greater than zero.
This means OpenAI will choose an output amount many possible choices.
Setting `n = 2` will instruct OpenAI to return two revisions and CopyAId will save
and open multiple revisions along with the source. For instance, a CopyAId task
involving `vimdiff` will display a 3-way diff.

Is no `seed` setting is set, these choices will be randomly chosen. The higher the
tempurature the more different the revisions will be from each other and the original
source.

Using setting such as this one can evaluate whether to incorporate a revision depending
on whether OpenAI suggests that revision in both randomly generated revisions.

```
[openai]
n = 2
temperature = 0.125
```

A higher temperature such as `0.5` can be useful for exploring very different rewording
of text and new ideas for major revisions. Similar effects can be achieved by changing
the wording of the prompt, for instance, by changing wording in the prompts from
"revise" to "rewrite".


Examples
--------

The following are examples of request settings files for "hot" copyediting:

* [hot-revise-gpt3.toml](examples/hot-revise-gpt3.toml) using GPT-3 and
* [hot-revise-gpt4.toml](examples/hot-revise-gpt4.toml) using GPT-4.

You add a new task to your configuration file to point to a copy of these examples.
For more details, see the [configuration page](config.md).
