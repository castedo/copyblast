Despite the continent's remoteness, human activity has a significant effect on it via [pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
