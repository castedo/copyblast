According to the terms of the treaty, military activity, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal) are all prohibited in Antarctica.
