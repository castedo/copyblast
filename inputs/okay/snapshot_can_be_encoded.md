A snapshot can be a file or a directory encoded for compatibility with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org).
