Running Baseprinter in a container requires downloading and caching
approximately one gigabyte of container image data.
In contrast, a local installation uses significantly less disk space.
