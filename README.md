The Python scripts for sending test inputs and prompts to OpenAI and saving their
outputs are now found in the `copyblaster` repository.

This repository is now a dumping ground for test inputs, test prompts and settings, and
their saved outputs.
