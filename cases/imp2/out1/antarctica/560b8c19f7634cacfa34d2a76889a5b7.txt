Approximately 70% of the world's freshwater reserves are locked in the ice of Antarctica. If this ice were to melt, it would cause global sea levels to rise by nearly 60 meters (200 feet).
