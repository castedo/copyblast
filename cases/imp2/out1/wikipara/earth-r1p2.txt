The Earth has a dynamic atmosphere that
sustains the planet's surface conditions and shields
it from most meteoroids and ultraviolet light upon entry.
Its composition is primarily nitrogen and oxygen.
Water vapor is also prevalent in the atmosphere,
forming clouds that envelop much of the Earth.
This water vapor acts as a greenhouse gas and,
along with other greenhouse gases like
carbon dioxide (CO₂),
traps
energy from the Sun's light.
This trapping of energy maintains the Earth's average surface temperature at 14.76 °C,
a level at which water can exist as a liquid under atmospheric pressure.

The amount of energy captured varies between geographic regions,
with the equatorial region receiving more sunlight than the polar regions. This differential heating drives atmospheric and ocean currents, which in turn produce a global climate system with distinct climate regions. It also gives rise to a variety of weather phenomena, including precipitation,
and enables the cycling of elements such as nitrogen.
