Earth, like most celestial bodies in the Solar System,
formed approximately 4.5 billion years ago from the gas present in the early Solar System.
During Earth's first billion years,
the ocean emerged, and life began to develop within it.
Life then spread across the globe, significantly altering Earth's atmosphere and surface,
which led to the Great Oxidation Event around two billion years ago.
Humans appeared 300,000 years ago in Africa and
have since populated every continent except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival,
but their increasing impact on the planet's environment
is unsustainable.
This impact threatens the well-being of humans and numerous other life forms,
leading to widespread extinctions.
