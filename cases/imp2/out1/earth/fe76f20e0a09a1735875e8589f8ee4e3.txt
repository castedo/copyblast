The Moon's gravity not only helps stabilize Earth's axis but also generates tides that gradually slow down Earth's rotation.
