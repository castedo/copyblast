This process maintains the current average surface temperature of 14.76°C, which allows water to remain liquid under atmospheric pressure.
