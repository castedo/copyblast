The Earth has a liquid outer core that generates a magnetosphere, which is capable of deflecting most of the destructive solar winds and cosmic radiation.
