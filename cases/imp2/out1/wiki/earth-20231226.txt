Earth is the third planet from the Sun and the only known astronomical object to harbor life.
This is possible because Earth is a water world, unique in the Solar System for sustaining liquid surface water.
Almost all of Earth's water is contained in its global ocean, which covers 70.8% of the planet's crust.
The remaining 29.2% of the crust is land, with most of it located in the form of continental landmasses within one hemisphere, known as Earth's land hemisphere.

Most of Earth's land is relatively humid and covered by vegetation, while the large ice sheets in Earth's polar deserts hold more water than the planet's groundwater, lakes, rivers, and atmospheric water combined.
The crust of Earth consists of slowly moving tectonic plates, which interact to produce mountain ranges, volcanoes, and earthquakes.
Earth has a liquid outer core that generates a magnetosphere, capable of deflecting most of the destructive solar wind and cosmic radiation.

Earth's dynamic atmosphere sustains the planet's surface conditions and protects it from most meteoroids and UV light upon entry.
It
is composed primarily of nitrogen and oxygen, with water vapor widely present, forming clouds that cover most of the planet.
Water vapor, acting as a greenhouse gas along with other gases in the atmosphere, particularly carbon dioxide (CO₂), traps energy from the Sun's light.
This process maintains the current average surface temperature of 14.76 °C, allowing both liquid surface water and water vapor to persist under atmospheric pressure.
The variation in energy capture between geographic regions, such as the equatorial region receiving more sunlight than the polar regions, drives atmospheric and ocean currents. This produces a global climate system with different climate regions and a range of weather phenomena, such as precipitation, enabling elements like nitrogen to cycle.

Earth is shaped into an ellipsoid with a circumference of about 40,000 km and
is the densest planet in the Solar System.
Among the four rocky planets, it is the largest and most massive.
Earth is approximately eight light-minutes from the Sun and orbits it, taking about 365.25 days to complete one revolution.
The planet rotates on its axis in just under a day, specifically about 23 hours and 56 minutes.
Earth's axis of rotation is tilted with respect to the perpendicular of its orbital plane around the Sun, resulting in the seasons.

Earth is orbited by one permanent natural satellite, the Moon, which is 384,400 km (1.28 light seconds) away and is roughly a quarter of
Earth's diameter. The Moon's gravity helps stabilize Earth's axis and causes tides, which gradually slow Earth's rotation.
Due to tidal locking, the same side of the Moon always faces Earth.

Earth, like most other bodies in the Solar System, formed 4.5 billion years ago from the gas in the early Solar System.
During the first billion years of Earth's history, the ocean formed, and life developed within it.
Life spread globally and has been altering Earth's atmosphere and surface, leading to the Great Oxidation Event two billion years ago.
Humans emerged 300,000 years ago in Africa and have since spread to every continent except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival but have increasingly impacted the planet's environment.
Humanity's current impact on Earth's climate and biosphere is unsustainable, threatening the well-being of humans and many other life forms, and causing widespread extinctions.
