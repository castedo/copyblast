First, create a new repository using a repository template.
This new repository will include:

- Source text files in LaTeX format.
- A GitHub Actions workflow file that automatically generates Baseprint snapshots and
  previews.

These files will be copied from the
[Baseprinter repository template](https://github.com/castedo/basecastbot-example/).
The workflow file can be found at `.github/workflows/pages-deploy.yaml`.

[Create repository from the template](
https://github.com/new?template_owner=castedo&template_name=basecastbot-example
){ .md-button .md-button--primary target='_blank' }

!!! warning
    As of October 2023, the Baseprinter workflow only saves a *preview* of the Baseprint snapshot.
    The Baseprint snapshot itself is temporarily generated solely for rendering a preview on
GitHub Pages.
    An upgrade to Baseprinter will also save a Baseprint snapshot in the Git repository.
