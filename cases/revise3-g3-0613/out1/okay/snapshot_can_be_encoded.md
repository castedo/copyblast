A snapshot can be a file or directory encoded to be compatible with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org).
