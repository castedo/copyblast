A snapshot can be a file or a directory encoded to be compatible with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org) [@cosmo_referencing_2020].
