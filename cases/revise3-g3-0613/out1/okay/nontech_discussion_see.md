For a non-technical discussion about Baseprint documents and their purpose,
refer to [Why Publish Baseprint Documents](https://perm.pub/wk1LzCaCSKkIvLAYObAvaoLNGPc).
