Humans rely on Earth's biosphere and natural resources for survival, yet their activities have increasingly affected the planet's environment.
