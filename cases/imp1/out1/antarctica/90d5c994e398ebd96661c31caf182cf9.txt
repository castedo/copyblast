Despite the continent's remoteness, human activity significantly affects it through pollution, ozone depletion, and climate change.
