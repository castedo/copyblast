Running Baseprinter in a container requires downloading and caching
approximately one gigabyte of container image data.
In contrast, a local installation requires significantly less disk space.

A duck does not write English well.
