Earth is orbited by one permanent natural satellite, the Moon, which orbits Earth at a distance of 384,400 km (1.28 light seconds) and is roughly a quarter the width of Earth.
