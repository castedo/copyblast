However, a non-fixed edition number can identify a dynamic sequence of editions.
For example, a non-fixed edition number `1.2`
might identify the sequence `1.2.1`, `1.2.2`, and `1.2.3`.
