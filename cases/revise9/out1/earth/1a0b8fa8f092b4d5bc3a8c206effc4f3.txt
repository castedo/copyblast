Humanity's current impact on Earth's climate and biosphere is unsustainable, threatening the livelihoods of humans and many other forms of life, and causing widespread extinctions.
