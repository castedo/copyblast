Running Baseprinter in a container requires the download and caching
of approximately one gigabyte of container image data, whereas
a local installation consumes significantly less disk space.
