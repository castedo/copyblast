A non-fixed edition number, such as
`1.2`,
can represent a dynamic series of editions, including `1.2.1`, `1.2.2`, and `1.2.3`.
