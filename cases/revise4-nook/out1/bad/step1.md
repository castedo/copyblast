First, create a new repository using a repository template.
This new repository will include:

- LaTeX format source text files
- A GitHub Actions workflow file that automatically generates Baseprint snapshots and
  previews

These files will be sourced from the
[Baseprinter repository template](https://github.com/castedo/basecastbot-example/).
You can find the workflow file at `.github/workflows/pages-deploy.yaml`.

To create a repository from the template, click the following link:

[Create repository from the template](
https://github.com/new?template_owner=castedo&template_name=basecastbot-example
){ .md-button .md-button--primary target='_blank' }

!!! warning
    As of October 2023, the Baseprinter workflow only creates a *preview* of the Baseprint snapshot.
    The Baseprint snapshot itself is generated temporarily to render a preview for
    GitHub Pages.
A future upgrade to Baseprinter will allow for saving a Baseprint snapshot in the Git repository.
