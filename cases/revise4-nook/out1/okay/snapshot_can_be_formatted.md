A snapshot can be either a file or a directory formatted to be compatible with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org).
