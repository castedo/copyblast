The quickest way to get started is to use
[`baseprint-starter`](https://github.com/castedo/baseprint-starter/)
as a template to [create a new repository](
https://github.com/new?template_owner=castedo&template_name=baseprint-starter
).
