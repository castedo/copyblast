The Moon's gravity helps stabilize Earth's axis, and also causes tides which gradually slow Earth's rotation.
