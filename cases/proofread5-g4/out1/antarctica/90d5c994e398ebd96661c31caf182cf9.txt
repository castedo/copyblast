Despite the continent's remoteness, human activity has a significant effect on it through pollution, ozone depletion, and climate change.
