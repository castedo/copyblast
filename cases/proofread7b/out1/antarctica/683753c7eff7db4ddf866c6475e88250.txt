During the summer months, about 5,000 people reside at research stations, a figure that drops to around 1,000 in the winter.
