Despite the continent's remoteness, human activity has a significant effect on it via pollution, ozone depletion, and climate change.
