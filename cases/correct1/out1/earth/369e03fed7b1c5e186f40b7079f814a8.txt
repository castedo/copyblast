Earth's axis of rotation is tilted with respect to the perpendicular to its orbital plane around the Sun, producing seasons.
