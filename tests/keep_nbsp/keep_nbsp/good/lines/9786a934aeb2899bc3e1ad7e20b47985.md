This process maintains the current average surface temperature of 14.76&nbsp;°C, at which water is liquid under atmospheric pressure.
