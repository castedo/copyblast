
This process maintains the current average surface temperature of 14.76 °C, at which water is liquid under atmospheric pressure.

Earth is rounded into an ellipsoid with a circumference of about 40,000 km.

Earth is orbited by one permanent natural satellite, the Moon, which orbits Earth at 384,400 km (1.28 light seconds) and is roughly a quarter as wide as Earth.

