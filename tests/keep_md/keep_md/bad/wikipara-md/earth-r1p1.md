Earth is the third planet from the Sun and
the only astronomical object known to harbor life.
This is enabled by Earth being a water world,
the only one in the Solar System sustaining liquid surface water.

covering 70.8% of Earth's crust.

most of which is located in the form of continental landmasses within one hemisphere,
Earth's land hemisphere.
Most of Earth's land is somewhat humid and covered by vegetation,
while large sheets of ice at Earth's polar deserts retain more water than
Earth's groundwater, lakes, rivers and atmospheric water combined.
Earth's crust consists of slowly moving tectonic plates,
which interact to produce mountain ranges, volcanoes, and earthquakes.
Earth has a liquid outer core that generates a magnetosphere
capable of deflecting most of the destructive solar winds and cosmic radiation.
