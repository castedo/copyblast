Earth is rounded into an ellipsoid with a circumference of about 40,000 km.
It is the densest planet in the Solar System.
Of the four rocky planets,

Earth is about eight light-minutes away from the Sun and orbits it,

Earth rotates around its own axis in slightly less than a day

Earth's axis of rotation is tilted


Earth is orbited by one permanent natural satellite, the Moon,


and also causes tides which gradually slow Earth's rotation.
As a result of tidal locking,

