Earth is shaped like an ellipsoid with a circumference of approximately 40,000 km.
It is the most dense planet in the Solar System.
Among the four terrestrial planets,
Earth is the largest and has the greatest mass. Earth is situated about eight light-minutes from the Sun and completes an orbit around it every
year, which takes roughly 365.25 days.
The planet rotates on its axis in just under a day,
specifically about 23 hours and 56 minutes.
The tilt of Earth's axis of rotation,
in relation to the perpendicular of its orbital plane around the Sun,
is responsible for the changing seasons.

Our planet is accompanied by a single permanent natural satellite, the Moon,
which circles Earth at a distance of 384,400 km, equivalent to 1.28 light seconds, and has
a diameter about one-quarter that of Earth.
The gravitational pull of the Moon not only helps to stabilize Earth's axial tilt but
also generates tides that gradually decelerate Earth's rotation.
Due to tidal locking,
we always see the same face of the Moon from Earth.
