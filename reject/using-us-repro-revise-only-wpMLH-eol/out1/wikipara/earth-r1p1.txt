Earth is the third planet from the Sun and
the only known celestial body to support life.
Its status as a water world,
unique in the Solar System for maintaining liquid surface water, facilitates this.
Earth's vast global ocean holds nearly all its water,
enveloping 70.8% of the planet's surface.
The remaining 29.2% is land, primarily found as continental masses predominantly in one hemisphere, known as Earth's land hemisphere.

The majority of Earth's land features a relatively humid climate and is blanketed with vegetation. In contrast,
the polar deserts' massive ice sheets contain more water than the combined total of Earth's
groundwater, lakes, rivers, and atmospheric moisture.
The planet's crust is composed of tectonic plates that move slowly,
their interactions giving rise to mountains, volcanoes, and seismic activity.

Encircling Earth is a liquid outer core that generates a magnetic field, forming a magnetosphere. This shield is crucial as it wards off the majority of harmful solar winds and cosmic radiation.
