Antarctica is the coldest, driest, and windiest continent on
average, and it has the highest average elevation.
It's largely a polar desert,
receiving more than 200 millimeters (8 inches) of precipitation annually along the coast and much less inland.
Approximately 70% of the Earth's freshwater reserves are locked in Antarctica's ice, which, if melted, would cause global sea levels to rise by nearly 60 meters (200 feet).
Antarctica also holds the record for the lowest temperature ever recorded on Earth,
at -89.2 °C (-128.6 °F).
During the summer, coastal areas can experience temperatures above 10 °C (50 °F).
Native animal species include mites, nematodes, penguins, seals, and tardigrades.
Where there is vegetation, it is predominantly lichens or mosses.
