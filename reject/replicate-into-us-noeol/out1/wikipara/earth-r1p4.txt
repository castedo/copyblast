Earth, like most other celestial bodies in the Solar System,
formed 4.5 billion years ago from the gas present in the early Solar System.
During Earth's first billion years,
the oceans formed, and life began to develop within them.
Life then spread across the planet, altering Earth's atmosphere and surface,
which led to the Great Oxidation Event about two billion years ago.
Humans appeared 300,000 years ago in Africa and
have since populated every continent except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival,
but their increasing impact on the planet's environment has been significant.
The current human influence on Earth's climate and biosphere is unsustainable,
posing a threat to the well-being of humans and many other life forms,
and is causing widespread extinctions.
