Earth is the third planet from the Sun and
the only known astronomical object that supports life.
This is possible because Earth is a water world,
the only one in our Solar System with liquid water on its surface.
The vast majority of Earth's water is in its global ocean,
which covers 70.8% of the planet's crust.
The remaining 29.2% is land, primarily found in continental landmasses that are concentrated in one hemisphere, known as Earth's land hemisphere.

Most of Earth's land is relatively humid and covered with vegetation. In contrast,
the large ice sheets in Earth's polar deserts hold more water than
all of Earth's groundwater, lakes, rivers, and atmospheric water combined.
The planet's crust is made up of slowly shifting tectonic plates,
whose interactions create mountain ranges, volcanoes, and earthquakes.

Earth also has a liquid outer core that generates a magnetosphere. This magnetic field is crucial as it shields the planet from the majority of harmful solar winds and cosmic radiation.
