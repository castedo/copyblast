Earth is the third planet from the Sun and the only known astronomical object to support life.
Its status as a water world, unique in the Solar System for sustaining liquid surface water, makes this possible.
Earth's global ocean holds nearly all its water, covering 70.8% of the planet's crust.
The remaining 29.2% is land, primarily found as continental landmasses within one hemisphere, known as Earth's land hemisphere. The majority of Earth's land
is relatively humid and vegetated, while the polar deserts' ice sheets contain more water than the combined total of groundwater, lakes, rivers, and atmospheric moisture.
The crust of Earth is made up of slowly shifting tectonic plates, which create mountains, volcanoes, and earthquakes through their interactions.

Earth's liquid outer core generates a magnetosphere, which shields the planet from the majority of harmful solar winds and cosmic radiation. The dynamic atmosphere of Earth maintains surface conditions and offers protection against most meteoroids and UV light upon entry.
It
is composed mainly of nitrogen and oxygen, with water vapor also present, forming clouds that envelop much of the planet.
Water vapor, along with other greenhouse gases like carbon dioxide (CO₂), traps energy from the Sun's light,
enabling the persistence of liquid surface water and atmospheric water vapor. This greenhouse effect keeps the average surface temperature at 14.76 °C, suitable for liquid water under atmospheric pressure.
The uneven distribution of trapped energy across different regions, such as the equatorial region receiving more sunlight than the poles, drives atmospheric and ocean currents. This results in a global climate system with diverse climate regions and weather phenomena like precipitation, facilitating the cycling of elements like nitrogen.

Earth is shaped into an ellipsoid with a circumference of about 40,000 km and
is the densest planet in the Solar System.
Among the four terrestrial planets, it is the largest and most massive.
Located approximately eight light-minutes from the Sun, Earth orbits it once every 365.25 days.
The planet rotates on its axis in just under a day—about 23 hours and 56 minutes.
The tilt of Earth's rotational axis relative to its orbital plane around the Sun gives rise to the seasons.
Earth is accompanied by a single natural satellite, the Moon, which orbits at a distance of 384,400 km (1.28 light seconds) and has a diameter about a quarter that of Earth.
The Moon's gravitational pull stabilizes Earth's axial tilt and causes tides, which are gradually slowing Earth's rotation.
Due to tidal locking, we always see the same side of the Moon.

Earth, along with other bodies in the Solar System, formed 4.5 billion years ago from the early Solar System's gas.
In its first billion years, the ocean appeared, and life began within it. Life then spread across the globe, altering Earth's
atmosphere and surface, leading to the Great Oxidation Event two billion years ago.
Humans appeared 300,000 years ago in Africa and have since inhabited every continent except Antarctica.
While humans rely on Earth's biosphere and natural resources, their growing
impact on the environment is unsustainable, threatening the well-being of humans and many other life forms, and leading to widespread extinctions.
