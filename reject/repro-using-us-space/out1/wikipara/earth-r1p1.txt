Earth is the third planet from the Sun and
the only known astronomical object to support life.
This is possible because Earth is a water world,
the only one in the Solar System with liquid surface water.
The vast majority of Earth's water is found in its global ocean,
which covers 70.8% of the planet's crust.
The remaining 29.2% of the crust is land,
primarily in the form of continental masses that are concentrated in one hemisphere,
known as the land hemisphere.

Most of Earth's land is relatively humid and covered with vegetation.
The polar deserts at the poles have large ice sheets that hold more water than the combined total of Earth's
groundwater, lakes, rivers, and atmospheric water.
The crust of Earth is made up of slowly shifting tectonic plates,
whose interactions create mountain ranges, volcanoes, and earthquakes.
Earth possesses a liquid outer core that generates a magnetosphere,
 which is capable of deflecting the majority of harmful solar winds and cosmic radiation.
