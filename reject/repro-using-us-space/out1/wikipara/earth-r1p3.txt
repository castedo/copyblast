The Earth is shaped into an ellipsoid with a circumference of approximately 40,000 kilometers.
It is the most dense planet in the Solar System.
Among the four terrestrial planets,
Earth is the largest and has the most mass. The Earth is situated about eight light-minutes from the Sun and orbits it,
taking a year—roughly 365.25 days—to complete one orbit.
The Earth rotates on its axis in just under a day,
specifically about 23 hours and 56 minutes.
The axis of Earth's rotation is tilted in relation
to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.

The Earth is orbited by a single permanent natural satellite, the Moon,
which is at a distance of 384,400 kilometers, or 1.28 light seconds, and has
a diameter about one-quarter that of Earth.
The gravitational pull of the Moon helps to stabilize the Earth's axis
and also causes ocean tides, which gradually slow down the Earth's rotation.
Due to tidal locking,
the same hemisphere of the Moon always faces the Earth.
