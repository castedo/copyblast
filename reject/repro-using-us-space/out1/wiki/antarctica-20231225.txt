Antarctica is the southernmost continent on Earth and is the least populated. It is located almost entirely below the Antarctic Circle and is surrounded by the Southern Ocean, which is also known as the Antarctic Ocean. The geographic South Pole is situated on this continent.
Antarctica ranks as
the fifth-largest continent and is approximately 40% larger than Europe, covering an area of 14,200,000 square kilometers (5,500,000 square miles).
The Antarctic ice sheet, which has an average thickness of 1.9 kilometers (1.2 miles), covers most of the continent.

Antarctica is the coldest, driest, and windiest continent on average and has the highest average elevation.
It is primarily a polar desert, receiving more than 200 millimeters (8 inches) of precipitation annually along the coast and much less inland.
Approximately 70% of the world's freshwater reserves are locked in Antarctica's ice. If this ice were to melt, it would cause global sea levels to rise by nearly 60 meters (200 feet).
The lowest temperature ever recorded on Earth, -89.2°C (-128.6°F), was measured in Antarctica.
However, coastal regions can experience temperatures above 10°C (50°F) during the summer.
Native animal species include mites, nematodes, penguins, seals, and tardigrades.
Vegetation, where it exists, is mostly in the form of lichens or mosses.

The ice shelves of Antarctica were likely first sighted in 1820 by a Russian expedition led by Fabian Gottlieb von Bellingshausen and Mikhail Lazarev.
Subsequent decades saw additional exploration by French, American, and British expeditions.
The first confirmed landing on the continent was by a Norwegian team in 1895.
In the early 20th century, several expeditions ventured into the interior of the continent.
British explorers were the first to reach the magnetic South Pole in 1909, and Norwegian explorers were the first to reach the geographic South Pole in 1911.

About 30 countries govern Antarctica, all of which are signatories to the 1959 Antarctic Treaty System.
The treaty prohibits military activity, mining, nuclear explosions, and the disposal of nuclear waste on the continent.
Tourism, fishing, and research are the primary human activities in and around Antarctica.
During the summer months, approximately 5,000 people live at research stations, a number that decreases to about 1,000 during the winter.
Despite its remoteness, human activity significantly impacts Antarctica through pollution, ozone depletion, and climate change.
