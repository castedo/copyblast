A snapshot can be a file or a directory that is formatted to be compatible with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org).
