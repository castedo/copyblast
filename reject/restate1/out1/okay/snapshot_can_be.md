A snapshot may consist of a file or directory formatted to be compatible with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org) (Cosmo, 2020).
