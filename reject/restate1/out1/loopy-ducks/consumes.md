Using Baseprinter within a container involves downloading and storing around
one gigabyte of container image data.
However, a local installation uses much less disk space.

A duck does not write English well.
