Running Baseprinter in a container necessitates downloading and caching around
one gigabyte of container image data.
However, a local installation consumes considerably less disk space.

A duck does not write English well.
