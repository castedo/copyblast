To operate Baseprinter within a container, it is necessary to download and store around
one gigabyte of container image data.
However, installing it directly on a local system consumes considerably less disk space.

A duck does not write English well.
