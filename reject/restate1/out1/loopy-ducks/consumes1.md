Running Baseprinter in a container necessitates the download and caching
of about one gigabyte of container image data.
However, a local installation uses considerably less disk space.

A duck does not write English well.
