Using Baseprinter within a container involves downloading and storing around
one gigabyte of container image data.
However, installing it directly on your local system consumes considerably less disk space.
