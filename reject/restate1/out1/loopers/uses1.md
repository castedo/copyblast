Using Baseprinter within a container necessitates downloading and storing around
one gigabyte of container image data.
However, installing it locally consumes considerably less disk space.
