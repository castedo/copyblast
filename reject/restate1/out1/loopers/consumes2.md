Using Baseprinter within a container involves downloading and storing around
one gigabyte of container image data,
while installing it directly on your system uses much less disk space.
