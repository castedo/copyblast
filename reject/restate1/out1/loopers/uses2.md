Using Baseprinter within a container involves downloading and storing around
one gigabyte of container image data,
whereas a direct installation on your system consumes considerably less disk space.
