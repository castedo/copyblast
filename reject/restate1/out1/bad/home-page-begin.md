# Copy**AI**d.it: AI-Powered Copyediting from the Command Line

Copy**AI**d is an open-source command line interface (CLI) tool
that utilizes the OpenAI API for
[GPT](https://en.wikipedia.org/wiki/Generative_pre-trained_transformer),
a [Large Language Model](https://en.wikipedia.org/wiki/Large_language_model),
to copyedit text files. This process is significantly faster, more frequent, and less expensive than human
copyediting
by several orders of magnitude.
