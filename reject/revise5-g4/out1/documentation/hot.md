Hot Copyediting
===============

Copy**AI**d is installed with a single example request settings file: `cold-example.toml`.
On most Linux distributions, this will be installed at `~/.config/copyaid/cold-example.toml`.


Cold Requests
-------------

A "cold" request is characterized by the following settings:

```
[openai]
n = 1
temperature = 0
```

The `n = 1` setting directs OpenAI to return only one revision.
The `temperature = 0` setting prompts OpenAI to provide its single best choice for
the request.

The following is an excerpt from the
[OpenAI API reference](https://platform.openai.com/docs/api-reference/chat/create):

> What sampling temperature to use, between 0 and 2.
> Higher values like 0.8 will result in more random output,
> while lower values like 0.2 will produce more focused and deterministic results.


Hot Requests
------------

In contrast, hot requests have a `temperature` setting greater than zero.
This means OpenAI will generate output from a range of possible choices.
Setting `n = 2` instructs OpenAI to return two revisions, and Copy**AI**d will save
and open multiple revisions alongside the source. For example, a Copy**AI**d task
using `vimdiff` will show a 3-way diff.

If no `seed` setting is specified, these choices will be randomly selected. The higher the
temperature, the more varied the revisions will be from each other and the original
source.

Using settings like these allows you to evaluate whether to adopt a revision depending
on whether OpenAI suggests that revision in both randomly generated outputs.

```
[openai]
n = 2
temperature = 0.125
```

A higher temperature, such as `0.5`, can be useful for exploring significantly different rewordings
of text and new ideas for major revisions. Similar effects can be achieved by altering
the wording of the prompt, for example, by changing "revise" to "rewrite" in the prompts.


Examples
--------

Below are examples of request settings files for "hot" copyediting:

* [hot-revise-gpt3.toml](examples/hot-revise-gpt3.toml) using GPT-3, and
* [hot-revise-gpt4.toml](examples/hot-revise-gpt4.toml) using GPT-4.

You can add a new task to your configuration file to point to a copy of these examples.
For more details, see the [configuration page](config.md).
