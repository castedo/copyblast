# OpenAI API Request Settings

The request settings file contains parameters that are sent to the
[OpenAI API endpoint for chat completion](https://platform.openai.com/docs/api-reference/chat/create).
While most parameters have a direct correlation, some indirectly influence the values in the OpenAI request.

Upon executing `copyaid init`, the command generates a `cold-example.toml` request settings file and
a `copyaid.toml` configuration file.

Users have the option to modify the `prepend` value to alter the copyediting
instructions sent to OpenAI. This value is prefixed to the source text that needs revision.


## Hot vs Cold Requests

The `cold-example.toml` file is configured with the following settings:

```
[openai]
n = 1
temperature = 0
```

These *cold* settings instruct OpenAI to provide the single most likely revision.
Altering the `temperature` to a value closer to `1` prompts OpenAI to offer
a revision selected randomly from various possible revisions.
The `n` value specifies the number of candidate revisions to be stored.
Utilizing *hot* settings, such as `temperature = 0.5` and `n = 2`, can be beneficial for reviewing
multiple AI-generated suggestions and assessing the confidence level in those suggestions.
