# Copy**AI**d.it: AI-Powered Copyediting from the Command Line

Copy**AI**d.it is an open-source command-line interface (CLI) tool that copyedits text files using the OpenAI API for
[GPT](https://en.wikipedia.org/wiki/Generative_pre-trained_transformer),
a [Large Language Model](https://en.wikipedia.org/wiki/Large_language_model).
It offers frequent, rapid, and cost-effective [copyediting](https://en.wikipedia.org/wiki/Copy_editing) services,
significantly more affordable than human copyediting.
