# Configuration

To view help on the current configuration, use the command `copyaid -h`.

On POSIX systems, the default location for the configuration file is
`~/.config/copyaid/copyaid.toml`.
For detailed information on the TOML format, visit [toml.io](https://toml.io).

## Initial Configuration

After running `copyaid init`, a heavily commented initial configuration file is created.
It sets up some basic tasks:

* `it`: Creates a new API request and runs `vimdiff` on the resulting revisions and the original
source.

* `stomp`: Overwrites the source file with the revision from a new API request.

The following tasks do not create a new API request; they only operate on saved revisions
from previous API requests:

* `diff`: Runs `diff` on saved revisions.

* `vimdiff`: Runs `vimdiff` on saved revisions.

* `where`: Prints the file location(s) of the saved revision(s).

* `replace`: Overwrites the source file with the saved revision from a prior API request.


## Tasks

A task may include an optional request to the OpenAI API followed
by an optional chain of *react* commands.

The `request` value for each task specifies the settings file to be used for
OpenAI API requests. See the [request settings](requests.md) page.

The `react` value is a list of commands to run on the original source and
the saved results of an OpenAI API request. These results are
from the request of a task if specified, or from saved results of running another task.
The commands in the list are defined in the `[commands]` section.


## Shell Commands

The `[commands]` section defines the shell command line to execute. In bash:

* `"$0"` expands to the path of the source file, and
* `"$@"` expands to the saved revisions returned by the latest API request.


## OpenAI API Key

The `openai_api_key_file` value is an optional path to a file containing your
OpenAI API key. This path can be relative to the configuration file.
If this value is not provided, the `OPENAI_API_KEY` environment variable must be set.

## Logging

The `log_format` value is optional and can be set to `json` or `jsoml` to log the exact request sent and
response received from the OpenAI API.
On most Linux distributions, logs are saved in `~/.local/state/copyaid/log`.
JSOML logs are easier to read, diff, and copy-and-paste from than JSON logs.
If `jsoml` is chosen, you must install the `jsoml` Python package.

