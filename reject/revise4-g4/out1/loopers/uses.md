Running Baseprinter in a container requires downloading and caching
approximately one gigabyte of container image data, whereas
a local installation consumes significantly less disk space.
