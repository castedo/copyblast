# Copy**AI**d.it: Copyedit with AI from a CLI

AI technologies, such as OpenAI's [GPT](https://en.wikipedia.org/wiki/Generative_pre-trained_transformer) and [Large Language Models](https://en.wikipedia.org/wiki/Large_language_model) (LLMs),
are capable of frequent, rapid, and cost-effective [copyediting](https://en.wikipedia.org/wiki/Copy_editing).
Copy**AI**d.it is an open-source command-line interface (CLI) tool that utilizes the OpenAI API to
copyedit text files.


## Features

* Supports LaTeX, Markdown, and HTML text formats.
* Integrates with text file comparison tools like vimdiff for reviewing and merging AI revisions.
* Allows customization of copy-editing instructions sent to OpenAI.
* Enables simultaneous file comparisons across multiple AI revisions.
* Permits customization of programs that automatically run on AI revisions.


<div class="action-band" markdown>
[Get Started](start.md){ .md-button .md-button--primary }
</div>


## Examples

### Simple Example

```bash
$ echo "Use [this software](http://copyaid.it) to write English well." > doc.md
$ copyaid stomp doc.md
OpenAI request for doc.md
Saving to /tmp/copyaid
$ cat doc.md
Use [this software](http://copyaid.it) to write English well.
```

### Vimdiff Example

You can customize your configuration file `~/.config/copyaid/copyaid.toml` for various
workflows and OpenAI prompts. The following example uses
the default configuration file provided.

If you prefer using `vimdiff`, you can:

```bash
$ echo "Use [this software](http://copyaid.it) to write English well." > doc.md
$ copyaid it doc.md
OpenAI request for doc.md
Saving to /tmp/copyaid
2 files to edit
```
Vimdiff will then compare the original source with the revisions after the OpenAI request.

### Workflow Example

Here's an example of a multi-step workflow:

```bash
$ echo "Use [this software](http://copyaid.it) to write English well." > doc.md
$ copyaid check doc.md
OpenAI request for doc.md
Saving to /tmp/copyaid
Files doc.md and /tmp/copyaid/R1/doc.md differ
$ copyaid diff doc.md 
1c1
< Use [this software](http://copyaid.it) to write English well.
---
> Use [this software](http://copyaid.it) to write English well.
$ copyaid replace doc.md 
$ cat doc.md
Use [this software](http://copyaid.it) to write English well.
```


## Related

Inspired and heavily influenced by:

* [manubot-ai-editor](https://github.com/greenelab/manubot-ai-editor/)
* [A publishing infrastructure for AI-assisted academic authoring](https://doi.org/10.1101/2023.01.21.525030)

If you have a CLI tool you would like to be mentioned here,
contact [Castedo Ellerman](https://castedo.com).
