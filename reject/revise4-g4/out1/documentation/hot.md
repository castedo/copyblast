Hot Copyediting
===============

Copy**AI**d is installed with a single example request settings file: `cold-example.toml`.
On most Linux distributions, this will be located at `~/.config/copyaid/cold-example.toml`.


Cold Requests
-------------

A request is considered "cold" due to the following settings:

```
[openai]
n = 1
temperature = 0
```

The `n = 1` setting directs OpenAI to provide only one revision.
The `temperature = 0` setting prompts OpenAI to return its single best choice for
the request.

Here is an explanation of these settings from the
[OpenAI API reference](https://platform.openai.com/docs/api-reference/chat/create):

> What sampling temperature to use, between 0 and 2.
> Higher values like 0.8 will result in more random outputs,
> while lower values like 0.2 will produce more focused and deterministic results.


Hot Requests
------------

Conversely, hot requests are characterized by a `temperature` setting above zero.
This indicates that OpenAI will generate output from a range of possible choices.
Setting `n = 2` commands OpenAI to return two revisions, and Copy**AI**d will save
and display multiple revisions alongside the original text. For example, a Copy**AI**d task
using `vimdiff` will show a 3-way diff.

If no `seed` setting is specified, these choices will be randomly selected. The higher the
temperature, the more varied the revisions will be from each other and the original
text.

Settings like these allow users to evaluate
whether to adopt a revision based on whether OpenAI suggests that revision in both randomly generated outputs.

```
[openai]
n = 2
temperature = 0.125
```

A higher temperature, such as `0.5`, can be useful for generating significantly different rewordings of text and new ideas for major revisions. Similar effects can be achieved by altering the wording of the prompt, for example, by changing "revise" to "rewrite" in the prompts.


Examples
--------

Below are examples of request settings files for "hot" copyediting:

* [hot-revise-gpt3.toml](examples/hot-revise-gpt3.toml) using GPT-3, and
* [hot-revise-gpt4.toml](examples/hot-revise-gpt4.toml) using GPT-4.

You can add a new task to your configuration file to reference copies of these examples.
For more information, see the [configuration page](config.md).
