The Earth has a dynamic atmosphere that
sustains the planet's surface conditions and shields
it from the majority of meteoroids and ultraviolet light upon entry.
Its composition is mainly nitrogen and oxygen.
Water vapor is also prevalent in the atmosphere,
forming clouds that cover much of the Earth.
This water vapor acts as a greenhouse gas and,
along with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO2),
it creates the conditions necessary for both liquid surface water and
water vapor to exist by trapping energy from the sun's light.
This process keeps the average surface temperature at 14.76 degrees Celsius,
a temperature at which water remains liquid under atmospheric pressure.
Variations in the amount of energy trapped in different geographic areas, such as the equatorial region receiving more sunlight than the polar regions,
drive atmospheric and ocean currents. This results in a global climate system with distinct climate regions and a variety of weather phenomena, including precipitation,
which allows elements like nitrogen to cycle through the environment.
