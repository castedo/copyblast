# Copy**AI**d.it: Copyedit with AI from a CLI

Copy**AI**d.it is an open-source command line interface (CLI) tool
that copyedits text files using the OpenAI API for
[GPT](https://en.wikipedia.org/wiki/Generative_pre-trained_transformer),
a [Large Language Model](https://en.wikipedia.org/wiki/Large_language_model)
capable of frequent, rapid, and very inexpensive
[copyediting](https://en.wikipedia.org/wiki/Copy_editing)
(by many orders of magnitude compared to human copyediting).
