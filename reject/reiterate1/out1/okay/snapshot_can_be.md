A snapshot can either be a file or a directory that is formatted to be compatible with Git, SWHIDs, and
the [Software Heritage Archive](https://softwareheritage.org) (Cosmo Referencing, 2020).
