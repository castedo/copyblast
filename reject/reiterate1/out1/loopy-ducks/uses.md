Running Baseprinter in a container necessitates the download and caching
of approximately one gigabyte of container image data.
On the other hand, a local installation requires considerably less disk space.

A duck does not write English well.
