Using Baseprinter within a container necessitates the download and storage
of about one gigabyte of container image data.
However, installing it locally uses much less disk space.
