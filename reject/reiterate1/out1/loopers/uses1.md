Using Baseprinter within a container necessitates the download and storage
of about one gigabyte of container image data.
However, installing it locally consumes considerably less disk space.
