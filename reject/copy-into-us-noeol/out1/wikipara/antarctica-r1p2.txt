Antarctica is, on average, the coldest, driest, and windiest continent,
with the highest average elevation.
It is primarily a polar desert,
receiving more than 200 millimeters (8 inches) of precipitation annually along the coast and much less inland.
Approximately 70% of the world's freshwater reserves are locked in Antarctica's ice,
which, if melted, would cause global sea levels to rise by nearly 60 meters (200 feet).
Antarctica has recorded the lowest temperature on Earth at -89.2 degrees Celsius (-128.6 degrees Fahrenheit).
During the summer, coastal areas can experience temperatures above 10 degrees Celsius (50 degrees Fahrenheit).
Native animal species include mites, nematodes, penguins, seals, and tardigrades.
Where vegetation is present, it is predominantly in the form of lichens or mosses.
