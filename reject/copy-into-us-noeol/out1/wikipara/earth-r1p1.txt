Earth is the third planet from the Sun and
the only known astronomical object to support life.
This is possible because Earth is a water world,
the only one in the Solar System with liquid water on its surface.
Nearly all of Earth's water is found in its global ocean,
which covers 70.8% of the planet's surface.
The remaining 29.2% of the surface is land, with most of it forming continental landmasses primarily located in one hemisphere, known as Earth's land hemisphere.

Most of Earth's land is relatively humid and covered with vegetation,
while the large ice sheets in the polar deserts hold more water than the combined total of Earth's
groundwater, lakes, rivers, and atmospheric water.
The planet's surface is made up of slowly shifting tectonic plates,
which create mountain ranges, volcanoes, and earthquakes through their interactions.

Earth possesses a liquid outer core that generates a magnetic field, forming a magnetosphere.
This magnetosphere is capable of deflecting most of the harmful solar winds and cosmic radiation that would otherwise be destructive.
