Antarctica is governed by approximately 30 countries,
all of which are signatories to the 1959 Antarctic Treaty System.
Under the treaty's terms,
military operations, mining, nuclear explosions, and the disposal
of nuclear waste are all forbidden in Antarctica.
The primary human activities in and around Antarctica are tourism, fishing, and scientific research.
During the summer months,
around 5,000 people live at research stations,
a number that decreases to about 1,000 in the winter.
Despite the continent's isolation,
human activity significantly impacts it through
pollution, ozone layer depletion, and climate change.
