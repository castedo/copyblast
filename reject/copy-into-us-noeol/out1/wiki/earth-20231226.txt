Earth is the third planet from the Sun and the only astronomical object known to support life.
This is possible because Earth is a water world, the only one in the Solar System with liquid surface water.
Almost all of Earth's water is in its global ocean, which covers 70.8% of the Earth's surface.
The remaining 29.2% of the surface is land, mostly in the form of continental landmasses concentrated in one hemisphere, known as Earth's land hemisphere.

Most of Earth's land is relatively humid and covered with vegetation, while large ice sheets in the polar deserts hold more water than Earth's groundwater, lakes, rivers, and atmospheric water combined. The Earth's surface is made up
of slowly moving tectonic plates, which interact to form mountain ranges, volcanoes, and cause earthquakes.
Earth has a liquid outer core that generates a magnetic field, creating a magnetosphere that deflects most of the harmful solar wind and cosmic radiation.

Earth's dynamic atmosphere maintains surface conditions and protects it from most meteoroids and ultraviolet light upon entry.
It is composed mainly of nitrogen and oxygen.
Water vapor is common in the atmosphere, forming clouds that cover much of the planet.
Water vapor acts as a greenhouse gas and, along with other greenhouse gases like carbon dioxide (CO₂), traps energy from the Sun's light.
This process keeps the average surface temperature at 14.76 °C, allowing for liquid water and water vapor to exist under atmospheric pressure.
Variations in trapped energy across different regions (such as the equatorial region receiving more sunlight than the polar regions) drive atmospheric and ocean currents, creating a global climate system with various climate regions and weather phenomena like precipitation, enabling elements like nitrogen to cycle.

Earth is shaped into an ellipsoid with a circumference of about 40,000 km and
is the densest planet in the Solar System.
Among the four terrestrial planets, it is the largest and most massive.
Earth is approximately eight light-minutes from the Sun and orbits it, taking a year (about 365.25 days) to complete one revolution.
Earth rotates on its axis in just under a day (about 23 hours and 56 minutes).
The axis of rotation is tilted relative to the perpendicular of its orbital plane around the Sun, resulting in seasons.
Earth is orbited by one permanent natural satellite, the Moon, which is about 384,400 km (1.28 light seconds) away and roughly a quarter of
Earth's diameter. The Moon's gravity helps stabilize Earth's axis and causes tides, which gradually slow Earth's rotation.
Due to tidal locking, the same side of the Moon always faces Earth.

Earth, like most other objects in the Solar System, formed 4.5 billion years ago from the gas of the early Solar System.
In the first billion years of Earth's history, the ocean formed, and life began within it.
Life spread across the globe and has been changing Earth's atmosphere and surface, leading to the Great Oxidation Event two billion years ago.
Humans appeared 300,000 years ago in Africa and have since spread to every continent except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival but have increasingly affected the planet's environment.
Humanity's current impact on Earth's climate and biosphere is unsustainable, threatening the well-being of humans and many other life forms, and causing widespread extinctions.
