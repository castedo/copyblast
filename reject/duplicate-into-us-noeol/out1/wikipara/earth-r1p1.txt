Earth is the third planet from the Sun and
the only astronomical object known to harbor life.
This is made possible because Earth is a water world,
the only one in the Solar System with liquid surface water.
Nearly all of Earth's water is found in its global ocean,
which covers 70.8% of Earth's surface.
The remaining 29.2% of Earth's surface is land,
with the majority of it forming continental landmasses primarily located in one hemisphere,
known as Earth's land hemisphere.
Most of Earth's land is relatively humid and covered with vegetation,
while the large ice sheets in Earth's polar deserts hold more water than
Earth's groundwater, lakes, rivers, and atmospheric water combined.
Earth's surface is made up of slowly shifting tectonic plates,
which interact to create mountain ranges, volcanoes, and earthquakes.
Earth possesses a liquid outer core that generates a magnetic field, creating a magnetosphere
capable of deflecting most of the harmful solar winds and cosmic radiation.
