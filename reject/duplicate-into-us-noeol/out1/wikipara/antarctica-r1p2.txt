Antarctica is, on average, the coldest, driest, and windiest continent,
and it has the highest average elevation.
It is primarily a polar desert,
with annual precipitation exceeding 200 millimeters (8 inches) along the coast and much less inland.
Approximately 70% of the world's freshwater reserves are locked in Antarctica's ice,
which, if melted, would cause global sea levels to rise by nearly 60 meters (200 feet).
Antarctica holds the record for the lowest recorded temperature on Earth,
-89.2°C (-128.6°F).
The coastal areas can experience temperatures above 10°C (50°F) during the summer.
Native animal species include mites, nematodes, penguins, seals, and tardigrades.
Where vegetation is present, it is predominantly in the form of lichens or mosses.
