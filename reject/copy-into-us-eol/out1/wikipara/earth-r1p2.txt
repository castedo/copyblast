The Earth has a dynamic atmosphere that
sustains the planet's surface conditions and shields
it from most meteoroids and ultraviolet light upon entry.
Its composition is primarily nitrogen and oxygen.
Water vapor is also widely present in the atmosphere,
forming clouds that cover most of the Earth.
This water vapor acts as a greenhouse gas and,
along with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO2),
it creates conditions that allow both liquid surface water and
water vapor to persist by trapping energy from the sun's light.
This process maintains the current average surface temperature of 14.76 degrees Celsius (58.57 degrees Fahrenheit),
at which water remains liquid under atmospheric pressure.
Variations in the amount of trapped energy between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions, drive atmospheric and ocean currents. This results in a global climate system with different climate regions and a range of weather phenomena, such as precipitation,
which allows components like nitrogen to cycle.
