a [Large Language Model](https://en.wikipedia.org/wiki/Large_language_model)
(by many orders of magnitude compared to copyediting by humans).
