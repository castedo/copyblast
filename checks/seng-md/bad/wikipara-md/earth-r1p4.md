
formed 4.5 billion years ago from gas in the early Solar System.
During the first billion years of Earth's history,
the ocean formed and then life developed within it.

leading to the Great Oxidation Event two billion years ago.
Humans emerged 300,000 years ago in Africa and
have spread across every continent on Earth with the exception of Antarctica.
Humans depend on Earth's biosphere and natural resources for their survival,
but have increasingly impacted the planet's environment.
Humanity's current impact on Earth's climate and biosphere is unsustainable,

and causing widespread extinctions.
