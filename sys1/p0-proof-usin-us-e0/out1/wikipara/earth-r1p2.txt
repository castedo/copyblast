Earth has a dynamic atmosphere that
sustains its surface conditions and
protects it from most meteoroids and UV light upon entry.
It is composed primarily of nitrogen and oxygen.
Water vapor is widely present in the atmosphere,
forming clouds that cover most of the planet.
The water vapor acts as a greenhouse gas and,
together with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO₂),
creates the conditions for both liquid surface water and
water vapor to persist by capturing energy from the Sun's light.
This process maintains the current average surface temperature of 14.76 °C,
at which water is liquid under atmospheric pressure.
Differences in the amount of captured energy between geographic regions
(as with the equatorial region receiving more sunlight than the polar regions)
drive atmospheric and ocean currents,
producing a global climate system with different climate regions
and a range of weather phenomena such as precipitation,
allowing components such as nitrogen to cycle.
