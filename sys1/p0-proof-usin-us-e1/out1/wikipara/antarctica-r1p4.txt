Antarctica is governed by approximately 30 countries,
all of which are parties to the 1959 Antarctic Treaty System.
According to the terms of the treaty,
military activity, mining, nuclear explosions, and nuclear waste disposal
are all prohibited in Antarctica.
Tourism, fishing, and research are the main human activities in and around Antarctica.
During the summer months,
about 5,000 people reside at research stations,
a figure that drops to around 1,000 in the winter.
Despite the continent's remoteness,
human activity has a significant effect on it through
pollution, ozone depletion, and climate change.
