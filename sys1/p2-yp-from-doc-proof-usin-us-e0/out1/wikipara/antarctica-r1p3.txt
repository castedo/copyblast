The text provided is grammatically correct and does not contain any spelling errors.
However, for clarity and consistency, you might consider adding the word "the" before "geographic South Pole" to match the earlier mention of "the magnetic South Pole." Here is the revised text with that minor adjustment:

The ice shelves of Antarctica were probably first seen in 1820,
during a Russian expedition
led by Fabian Gottlieb von Bellingshausen and Mikhail Lazarev.
The decades that followed saw further exploration
by French, American, and British expeditions.
The first confirmed landing was by a Norwegian team in 1895.
In the early 20th century,
there were a few expeditions into the interior of the continent.
British explorers were the first to reach the magnetic South Pole in 1909,
and the geographic South Pole was first reached in 1911 by Norwegian explorers.
