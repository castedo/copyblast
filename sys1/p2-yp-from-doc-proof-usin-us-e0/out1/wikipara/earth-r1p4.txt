The provided text is largely correct, but there are a few minor adjustments that can be made for clarity and accuracy:

- The phrase "from gas in the early Solar System" could be more specific.
It's generally understood that the Earth and other bodies formed from a cloud of gas and dust.
- The phrase "the ocean formed and then life developed within it" could be slightly rephrased for clarity.
- The term "Great Oxidation Event" is a proper noun and should be capitalized.
- The phrase "Humans emerged 300,000 years ago in Africa and have spread across every continent on Earth with the exception of Antarctica" is correct, but it could be rephrased for better flow.
- The last sentence could be split into two for better readability.

Here is a revised version of the text:

Earth, like most other bodies in the Solar System,
formed 4.5 billion years ago from a cloud of gas and dust.
During the first billion years of Earth's history,
oceans formed, and life subsequently developed within them.
Life spread globally and has been altering Earth's atmosphere and surface,
leading to the Great Oxidation Event about two billion years ago.
Humans emerged around 300,000 years ago in Africa and
have since spread to every continent except Antarctica.
Humans depend on Earth's biosphere and natural resources for their survival
but have increasingly impacted the planet's environment.
Humanity's current impact on Earth's climate and biosphere is unsustainable
