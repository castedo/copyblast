The Earth has a dynamic atmosphere that
sustains the planet's surface conditions and
protects it from most meteoroids and ultraviolet (UV) light upon entry.
Its composition is primarily nitrogen and oxygen.
Water vapor is also widely present in the atmosphere,
forming clouds that cover much of the planet.
The water vapor acts as a greenhouse gas and,
along with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO2),
creates the conditions necessary for both liquid surface water and
water vapor to persist by capturing energy from the Sun's light.
This process maintains the current average surface temperature of 14.76°C,
at which water is liquid under atmospheric pressure.

Differences in the amount of captured energy between geographic regions (such as the equatorial region receiving more sunlight than the polar regions)
drive atmospheric and ocean currents.
This results in a global climate system with different climate regions
and a range of weather phenomena, such as precipitation,
allowing components such as nitrogen to cycle.
