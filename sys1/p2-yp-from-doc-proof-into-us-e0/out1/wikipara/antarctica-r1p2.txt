Antarctica is, on average, the coldest, driest, and windiest continent,
with the highest average elevation.
It is primarily a polar desert,
with annual precipitation of over 200 mm (8 in) along the coast and far less inland.
About 70% of the world's freshwater reserves are frozen in Antarctica,
which, if melted, would raise global sea levels by almost 60 meters (200 ft).
Antarctica holds the record for the lowest measured temperature on Earth,
−89.2 °C (−128.6 °F).
The coastal regions can reach temperatures above 10 °C (50 °F) in the summer.
Native species of animals include mites, nematodes, penguins, seals, and tardigrades.
Where vegetation occurs, it is mostly in the form of lichens or moss.
