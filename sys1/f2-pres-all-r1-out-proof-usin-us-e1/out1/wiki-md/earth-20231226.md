**Earth** is the third [planet](planet) from the [Sun](Sun) and the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
This is enabled by Earth being a [water world](ocean_world), the only one in the [Solar System](Solar_System) sustaining liquid [surface water](surface_water).
Almost all of Earth's water is contained in its global ocean, covering [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The remaining 29.2% of Earth's crust is land, most of which is located in the form of [continental](continent) [landmasses](landmass) within one [hemisphere](Hemispheres_of_Earth), Earth's [land hemisphere](land_hemisphere).
Most of Earth's land is somewhat [humid](humid) and covered by vegetation, while large [sheets of ice](Ice_sheet) at [Earth's polar](Earth's_polar_regions) [deserts](desert) retain more water than Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
Earth's crust consists of slowly moving [tectonic plates](plate_tectonics), which interact to produce mountain ranges, [volcanoes](volcano), and earthquakes.
[Earth has a liquid outer core](Earth's_outer_core) that generates a [magnetosphere](magnetosphere) capable of deflecting most of the destructive [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).

Earth has [a dynamic atmosphere](Atmosphere_of_Earth), which sustains Earth's surface conditions and protects it from most [meteoroids](meteoroid) and [UV light at entry](ozone_layer).
It has a composition of primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is widely present in the atmosphere, [forming clouds](Cloud#Formation) that cover most of the planet.
The water vapor acts as a [greenhouse gas](greenhouse_gas) and, together with other greenhouse gases in the atmosphere, particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>), creates the conditions for both liquid surface water and water vapor to persist via the capturing of [energy from the Sun's light](Solar_irradiance).
This process maintains the current average surface temperature of 14.76&nbsp;°C, at which water is liquid under atmospheric pressure.
Differences in the amount of captured energy between geographic regions (as with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions)) drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current), producing a global [climate system](climate_system) with different [climate regions](climate_region), and a range of weather phenomena such as [precipitation](precipitation), allowing components such as [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle).

Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of about 40,000&nbsp;km.
It is the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Of the four [rocky planets](terrestrial_planet), it is the largest and most massive.
Earth is about eight [light-minutes](light-minute) away from the Sun and [orbits it](Earth's_orbit), taking a year (about 365.25 days) to complete one revolution.
[Earth rotates](Earth's_rotation) around its own axis in slightly less than a day (in about 23 hours and 56 minutes).
[Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted with respect to the perpendicular to its orbital plane around the Sun, producing seasons.
Earth is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon), which orbits Earth at 384,400&nbsp;km (1.28 light seconds) and is roughly a quarter as wide as Earth.
The Moon's gravity helps stabilize Earth's axis, and also causes [tides](tide) which [gradually slow Earth's rotation](Tidal_acceleration).
As a result of [tidal locking](tidal_locking), the same side of the Moon always faces Earth.

Earth, like most other bodies in the Solar System, [formed 4.5 billion years ago](age_of_Earth) from gas in the [early Solar System](formation_and_evolution_of_the_Solar_System).
During the first [billion years](billion_years) of [Earth's history](history_of_Earth), the ocean formed and then [life developed](Abiogenesis) within it.
Life spread globally and has been altering Earth's atmosphere and surface, leading to the [Great Oxidation Event](Great_Oxidation_Event) two billion years ago.
Humans emerged [300,000 years ago](Human_history) in Africa and have spread across every continent on Earth with the exception of [Antarctica](Antarctica).
Humans depend on Earth's [biosphere](biosphere) and natural resources for their survival, but have [increasingly impacted the planet's environment](Human_impact_on_the_environment).
Humanity's current impact on Earth's climate and biosphere is [unsustainable](sustainability), threatening the livelihood of humans and many other forms of life, and [causing widespread extinctions](Holocene_extinction).
