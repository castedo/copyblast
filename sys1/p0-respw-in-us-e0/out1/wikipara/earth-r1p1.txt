Earth is the third planet from the Sun and
the only astronomical object known to harbor life.
This is made possible because Earth is a water world,
the only one in the Solar System with liquid surface water.
The vast majority of Earth's water is found in its global ocean,
which covers 70.8% of the planet's crust.
The remaining 29.2% of the crust is land, primarily in the form of continental landmasses that are concentrated in one hemisphere, known as Earth's land hemisphere.

Most of Earth's land is relatively humid and covered with vegetation.
In contrast, the large ice sheets found in Earth's polar deserts hold more water than
all of Earth's groundwater, lakes, rivers, and atmospheric water combined.
The planet's crust is made up of slowly moving tectonic plates that
interact with each other, creating mountain ranges, volcanoes, and causing earthquakes.

Beneath the surface, Earth has a liquid outer core that generates a magnetosphere.
This magnetic field is crucial for life on Earth as it deflects most of the harmful solar winds and cosmic radiation that would otherwise strip away the atmosphere and bombard the surface.
