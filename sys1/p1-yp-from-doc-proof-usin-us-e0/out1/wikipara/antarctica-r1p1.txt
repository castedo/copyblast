The provided text is already well-written and requires no corrections.
It adheres to the standards of American English.
Here is the proofread text, unchanged:

Antarctica is Earth's southernmost and least-populated continent.
Situated almost entirely south of the Antarctic Circle
and surrounded by the Southern Ocean
(also known as the Antarctic Ocean),
it contains the geographic South Pole.
Antarctica is the fifth-largest continent,
being about 40% larger than Europe,
and has an area of 14,200,000 km² (5,500,000 sq mi).
Most of Antarctica is covered by the Antarctic ice sheet,
with an average thickness of 1.9 km (1.2 mi).
