The provided text is largely correct, but there are a few minor adjustments that could be made for clarity and style:

- The phrase "Earth's land hemisphere" might be confusing to some readers.
It could be rephrased for clarity.
- The term "polar deserts" is not commonly used; "polar regions" or "polar ice caps" might be more familiar to readers.
- The phrase "retain more water than Earth's groundwater, lakes, rivers and atmospheric water combined" could be restructured for better flow.

Here is a revised version of the text:

Earth is the third planet from the Sun and the only astronomical object known to harbor life.
This is made possible by Earth being a water world,
the only one in the Solar System with liquid surface water.
Almost all of Earth's water is contained in its global ocean,
which covers 70.8% of the planet's surface.
The remaining 29.2% is land,
with most of it located in the form of continental landmasses primarily within one hemisphere,
known as the land hemisphere.

Most of Earth's land is relatively humid and covered by vegetation.
The large ice sheets in the polar regions hold more water than all of Earth's
groundwater, lakes, rivers, and atmospheric water combined.
Earth's crust is made up of slowly moving tectonic plates,
which interact to produce mountain ranges, volcanoes, and earthquakes.
The planet has a liquid outer core that generates a magnetosphere,
capable of deflecting most of the destructive solar wind and cosmic radiation.
