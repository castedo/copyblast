Earth is rounded into an ellipsoid with a circumference of about 40,000 km.
It is the densest planet in the Solar System.
Of the four rocky planets,
it is the largest and most massive.
Earth is about eight light-minutes away from the Sun and orbits it,
taking a year (about 365.25 days) to complete one revolution.
Earth rotates around its own axis in slightly less than a day
(in about 23 hours and 56 minutes).
Earth's axis of rotation is tilted
with respect to the perpendicular to its orbital plane around the Sun,
producing seasons.
Earth is orbited by one permanent natural satellite, the Moon,
which orbits Earth at 384,400 km (1.28 light seconds) and
is roughly a quarter as wide as Earth.
The Moon's gravity helps stabilize Earth's axis
and also causes tides which gradually slow Earth's rotation.
As a result of tidal locking,
the same side of the Moon always faces Earth.
