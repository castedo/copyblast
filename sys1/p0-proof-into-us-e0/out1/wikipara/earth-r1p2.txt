The Earth has a dynamic atmosphere that
sustains the planet's surface conditions and
protects it from most meteoroids and ultraviolet light upon entry.
Its composition is primarily nitrogen and oxygen.
Water vapor is also widely present in the atmosphere,
forming clouds that cover much of the planet.
This water vapor acts as a greenhouse gas and,
along with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO₂),
creates the conditions necessary for both liquid surface water and
water vapor to persist by capturing energy from the sun's light.
This process maintains the current average surface temperature of 14.76 °C,
at which water remains liquid under atmospheric pressure.

Differences in the amount of energy captured between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions, drive atmospheric and ocean currents.
This results in a global climate system with various climate regions and a range of weather phenomena, such as precipitation,
which allows components like nitrogen to cycle.
