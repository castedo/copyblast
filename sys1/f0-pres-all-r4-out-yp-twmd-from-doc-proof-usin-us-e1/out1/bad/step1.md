First, create a new repository from a repository template.
This new repository will contain:

- Source text files in LaTeX format, and
- A GitHub Actions workflow file that automatically generates Baseprint snapshots and
  previews.

These files will be copied from the
[Baseprinter repository template](https://github.com/castedo/basecastbot-example/).
The workflow file is located at `.github/workflows/pages-deploy.yaml`.

[Create repository from the template](
https://github.com/new?template_owner=castedo&template_name=basecastbot-example
){ .md-button .md-button--primary target='_blank' }

!!! warning
    As of Oct 2023, the Baseprinter workflow only saves a *preview* of the Baseprint snapshot.
    The Baseprint snapshot itself is only temporarily generated in order to render a preview for
    GitHub Pages.
    An upgrade to Baseprinter will also save a Baseprint snapshot in the Git repository.
