The Earth is shaped into an ellipsoid with a circumference of approximately 40,000 kilometers.
It is the densest planet in the Solar System.
Among the four terrestrial planets,
Earth is the largest and most massive.
It is situated about eight light-minutes from the Sun and orbits it,
taking a year (roughly 365.25 days) to complete one revolution.
The Earth rotates on its axis in just under a day
(approximately 23 hours and 56 minutes).
The axis of Earth's rotation is tilted relative
to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.

The Earth is orbited by one permanent natural satellite, the Moon,
which circles the Earth at a distance of 384,400 kilometers (1.28 light seconds) and
is about a quarter of the
Earth's diameter.
The Moon's gravitational pull helps stabilize the Earth's axis and also causes tides, which gradually slow the Earth's rotation.
Due to tidal locking,
the same side of the Moon always faces the Earth.
