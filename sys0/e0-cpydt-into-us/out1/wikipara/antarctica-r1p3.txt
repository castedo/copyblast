The ice shelves of Antarctica were likely first observed in 1820
during a Russian expedition
led by Fabian Gottlieb von Bellingshausen and Mikhail Lazarev.
The subsequent decades witnessed additional exploration
by French, American, and British expeditions.
The first confirmed landing occurred in 1895 by a Norwegian team.
In the early 20th century,
several expeditions ventured into the continent's interior.
British explorers were the first to reach the magnetic South Pole in 1909,
and the geographic South Pole was first reached by Norwegian explorers in 1911.
