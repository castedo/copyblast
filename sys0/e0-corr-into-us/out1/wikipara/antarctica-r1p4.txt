Antarctica is governed by approximately 30 countries,
all of which are signatories to the 1959 Antarctic Treaty System.
Under the terms of the treaty,
military activity, mining, nuclear explosions, and nuclear waste disposal
are all prohibited on the continent.
The primary human activities in and around Antarctica are tourism, fishing, and research.
During the summer months,
about 5,000 people live at research stations,
a number that decreases to around 1,000 in the winter.
Despite the continent's remote location,
human activity significantly impacts it through
pollution, ozone depletion, and climate change.
