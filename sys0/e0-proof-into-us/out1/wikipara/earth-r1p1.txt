Earth is the third planet from the Sun and
the only astronomical object known to harbor life.
This is made possible by Earth being a water world,
the only one in the Solar System with sustaining liquid surface water.
Almost all of Earth's water is contained in its global ocean,
which covers 70.8% of the Earth's crust.
The remaining 29.2% of the Earth's crust is land,
most of which is located in the form of continental landmasses within one hemisphere,
known as Earth's land hemisphere.

Most of Earth's land is somewhat humid and covered by vegetation,
while large sheets of ice in Earth's polar deserts retain more water than
Earth's groundwater, lakes, rivers, and atmospheric water combined.
Earth's crust consists of slowly moving tectonic plates,
which interact to produce mountain ranges, volcanoes, and earthquakes.
Earth has a liquid outer core that generates a magnetosphere
capable of deflecting most of the destructive solar winds and cosmic radiation.
