Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of about 40,000 km.
It is the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Of the four [rocky planets](terrestrial_planet),
it is the largest and most massive.
Earth is about eight [light-minutes](light-minute) away from the Sun and [orbits it](Earth's_orbit),
taking a year (about 365.25 days) to complete one revolution.
[Earth rotates](Earth's_rotation) around its own axis in just under a day
(in about 23 hours and 56 minutes).
[Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted
with respect to the perpendicular to its orbital plane around the Sun,
producing seasons.
Earth is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon),
which orbits Earth at 384,400 km (1.28 light seconds) and
is roughly a quarter the width of Earth.
The Moon's gravity helps stabilize Earth's axis
and also causes [tides](tide) which [gradually slow Earth's rotation](Tidal_acceleration).
As a result of [tidal locking](tidal_locking),
the same side of the Moon always faces Earth.
