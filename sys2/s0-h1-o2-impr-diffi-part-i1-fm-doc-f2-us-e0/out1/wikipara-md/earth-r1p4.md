Earth, like most other bodies in the Solar System,
[formed 4.5 billion years ago](age_of_Earth) from gas in the [early Solar System](formation_and_evolution_of_the_Solar_System).
During the first [billion years](billion_years) of [Earth's history](history_of_Earth),
the ocean formed and then [life developed](Abiogenesis) within it.
Life spread globally and has been altering Earth's atmosphere and surface,
leading to the [Great Oxidation Event](Great_Oxidation_Event) two billion years ago.
Humans emerged [300,000 years ago](Human_history) in Africa and
have spread across every continent on Earth, with the exception of [Antarctica](Antarctica).
Humans rely on Earth's [biosphere](biosphere) and natural resources for their survival,
but have [increasingly impacted the planet's environment](Human_impact_on_the_environment).
Humanity's current impact on Earth's climate and biosphere is [unsustainable](sustainability),
threatening the well-being of humans and many other forms of life,
and [causing widespread extinctions](Holocene_extinction).
