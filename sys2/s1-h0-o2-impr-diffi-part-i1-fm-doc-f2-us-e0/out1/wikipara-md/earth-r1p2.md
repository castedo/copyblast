The
Earth's atmosphere is dynamic,
sustaining the planet's surface conditions
and shielding it from
the majority of meteoroids and harmful UV radiation.
Composed predominantly of nitrogen and oxygen,
the atmosphere also contains water vapor, which contributes to cloud formation that blankets much of the Earth.
Water vapor, along with other greenhouse gases—most notably carbon dioxide (CO<sub>2</sub>)—traps energy from the Sun,
a process essential for maintaining
the presence of both liquid water and water vapor.
This greenhouse effect keeps the average surface temperature at a life-sustaining 14.76&nbsp;°C,
under which water remains liquid at atmospheric pressure.

Geographical disparities in energy absorption, such as the equatorial region receiving more sunlight than the polar regions,
propel atmospheric and ocean currents.
These currents are integral to a global climate system characterized by diverse climate regions and a spectrum of weather events, including precipitation.
Such dynamics enable essential elements like nitrogen to participate in biogeochemical cycles.
