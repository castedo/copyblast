The Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and shields
it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also prevalent in the atmosphere,
[forming clouds](Cloud#Formation) that envelop much of the planet.
Water vapor functions as a [greenhouse gas](greenhouse_gas),
and along with other greenhouse gases,
especially [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it traps
[energy from the Sun's light](Solar_irradiance).
This trapping of energy sustains the current average surface temperature of 14.76&nbsp;°C,
a temperature at which water remains liquid under atmospheric pressure.

Variations in the amount of energy captured in different geographic areas (such as the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions)) propel
[atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
leading to a global [climate system](climate_system) with distinct [climate regions](climate_region).
This system gives rise to a variety of weather phenomena, including [precipitation](precipitation),
and enables elements like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through the environment.
