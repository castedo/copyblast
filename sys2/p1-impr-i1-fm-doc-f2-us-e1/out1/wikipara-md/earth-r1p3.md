The Earth is shaped into an ellipsoid due to [hydrostatic equilibrium](Hydrostatic_equilibrium) and has a [circumference](Earth's_circumference) of approximately 40,000 km.
It holds the title of the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Among the four [rocky planets](terrestrial_planet),
Earth stands out as the largest and most massive.
Our planet is situated about eight [light-minutes](light-minute) from the Sun and [orbits](Earth's_orbit) it,
taking about 365.25 days to complete one full revolution.

The [rotation of the Earth](Earth's_rotation) on its axis takes just under a day—approximately
23 hours and 56 minutes.
The [axis of Earth's rotation](#Axial_tilt_and_seasons) is inclined relative
to the perpendicular of its orbital plane around the Sun,
which gives rise to the seasons.

Our planet is accompanied by a single [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon),
which circles Earth at a distance of 384,400 km (1.28 light seconds) and has
a diameter about one-quarter that of Earth.
The gravitational pull of the Moon not only helps to stabilize Earth's axial tilt but
also generates [tides](tide) that [gradually slow down the rotation of the Earth](Tidal_acceleration).
Due to [tidal locking](tidal_locking),
we always see the same face of the Moon from Earth.
