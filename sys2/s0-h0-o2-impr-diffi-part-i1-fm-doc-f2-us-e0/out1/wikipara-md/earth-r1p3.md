Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of approximately 40,000&nbsp;km.
It is the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Among the four [rocky planets](terrestrial_planet),
Earth is the largest and most massive.
Earth is situated about eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit),
completing one revolution approximately every 365.25 days, which constitutes a year.
[Earth rotates](Earth's_rotation) on its axis in just under a day
(taking about 23 hours and 56 minutes).
[Earth's axis of rotation](#Axial_tilt_and_seasons) is inclined
relative to the perpendicular of its orbital plane around the Sun,
resulting in the occurrence of seasons.
Earth is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon),
which circles Earth at a distance of 384,400&nbsp;km (1.28 light seconds) and
has a diameter approximately one-quarter that of Earth.
The Moon's gravitational pull contributes to the stabilization of Earth's axis
and also generates [tides](tide), which [gradually decelerate Earth's rotation](Tidal_acceleration).
Due to [tidal locking](tidal_locking),
the Moon consistently presents the same hemisphere to Earth.
