During Earth's initial billion years,
the ocean emerged, followed by the advent of life within its waters.
Life proliferated across the planet, significantly transforming Earth's atmosphere and terrain,
culminating in the Great Oxidation Event approximately two billion years ago.
Humans appeared around 300,000 years ago in Africa and
have since inhabited every continent except Antarctica.
Our reliance on Earth's biosphere and natural resources is critical for survival,
yet our growing influence
on
the environment is unsustainable,
jeopardizing the well-being of humans and numerous other life forms,
and leading to extensive species extinctions.
