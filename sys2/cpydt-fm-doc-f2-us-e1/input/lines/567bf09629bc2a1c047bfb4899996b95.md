Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of approximately 40,075 kilometers.
