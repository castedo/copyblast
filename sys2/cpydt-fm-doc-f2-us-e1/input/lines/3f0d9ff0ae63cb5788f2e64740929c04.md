Earth is approximately eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit), taking a year (about 365.25 days) to complete one revolution.
