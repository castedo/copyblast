Most of Earth's land is somewhat [humid](humid) and covered by vegetation.
In contrast, the large [ice sheets](Ice_sheet) at [Earth's polar regions](Earth's_polar_regions) and [deserts](desert) contain less water than Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
