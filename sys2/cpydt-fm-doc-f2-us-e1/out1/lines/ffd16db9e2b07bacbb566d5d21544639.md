The Moon's gravity helps stabilize Earth's axis and also causes [tides](tide), which [gradually slow Earth's rotation](Tidal_acceleration).
