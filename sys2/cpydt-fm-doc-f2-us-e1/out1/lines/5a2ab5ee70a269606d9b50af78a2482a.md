This is enabled by Earth being a [water world](ocean_world), the only one in the [Solar System](Solar_System) that sustains liquid [surface water](surface_water).
