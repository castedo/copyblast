[Water vapor](Water_vapor) is widely present in the atmosphere, [forming clouds](Cloud#Formation) that cover much of the planet.
