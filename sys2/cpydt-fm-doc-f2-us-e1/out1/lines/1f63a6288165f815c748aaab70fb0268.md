During the first [billion years](billion_years) of [Earth's history](history_of_Earth), the oceans formed, and then [life developed](Abiogenesis) within them.
