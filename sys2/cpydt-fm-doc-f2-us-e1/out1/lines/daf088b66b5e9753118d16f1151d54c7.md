About 70% of the world's [freshwater](freshwater) reserves are frozen in Antarctica, which, if melted, would raise global [sea levels](sea_level) by nearly 60 meters (200 feet).
