It is primarily a [polar desert](polar_desert), with annual [precipitation](Climate_of_Antarctica#Precipitation) exceeding 200 mm (8 in) along the coast and far less inland.
