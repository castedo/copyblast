The [ice shelves of Antarctica](Antarctic_ice_sheet) were likely first observed in 1820
during [a Russian expedition](First_Russian_Antarctic_Expedition)
led by [Fabian Gottlieb von Bellingshausen](Fabian_Gottlieb_von_Bellingshausen) and [Mikhail Lazarev](Mikhail_Lazarev).
The subsequent decades witnessed additional [exploration](List_of_Antarctic_expeditions)
by French, American, and British expeditions.
The first confirmed landing occurred in 1895 by a Norwegian team.
In the early 20th century,
several expeditions ventured into the continent's interior.
[British explorers](Nimrod_Expedition) were the first to reach the [magnetic South Pole](South_magnetic_pole) in 1909,
and the [geographic South Pole](Geographic_south_pole) was first reached in 1911 by [Norwegian explorers](Amundsen's_South_Pole_expedition).
