Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and
protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is widely present in the atmosphere,
[forming clouds](Cloud#Formation) that cover much of the planet.
The water vapor acts as a [greenhouse gas](greenhouse_gas),
and together with other greenhouse gases in the atmosphere,
particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it creates conditions that allow both liquid surface water and
water vapor to persist by capturing [energy from the Sun's light](Solar_irradiance).
This process maintains the current average surface temperature of 14.76&nbsp;°C,
at which water remains liquid under atmospheric pressure.

Differences in the amount of captured energy between geographic regions (with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions))
drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
producing a global [climate system](climate_system) with various [climate regions](climate_region).
This system gives rise to a range of weather phenomena, such as [precipitation](precipitation),
and allows components like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through the environment.
