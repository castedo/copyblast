Earth has a [dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and
protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also widely present,
[forming clouds](Cloud#Formation) that cover much of the planet.
This water vapor acts as a [greenhouse gas](greenhouse_gas) and,
along with other greenhouse gases in the atmosphere,
particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
traps
[energy from the Sun's light](Solar_irradiance).
This process maintains the current average surface temperature of 14.76&nbsp;°C,
which allows water to remain liquid under atmospheric pressure.
Variations in the amount of trapped energy between geographic regions
(with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions))
drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
producing a global [climate system](climate_system) with distinct [climate regions](climate_region).
This system gives rise to a range of weather phenomena, such as [precipitation](precipitation),
and allows components like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through the environment.
