Earth is shaped into an ellipsoid with a circumference of about 40,000&nbsp;km.
It is the densest planet in the Solar System.
Among the four rocky planets,
Earth is the largest and most massive.
Earth is approximately eight light-minutes from the Sun and orbits it,
taking about 365.25 days to complete one revolution.
Earth rotates on its axis in just under a day
(approximately 23 hours and 56 minutes).
Earth's axis of rotation is tilted
with respect to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.
Earth is orbited by one natural satellite, the Moon,
which is about 384,400&nbsp;km (1.28 light seconds) away and
is roughly one-quarter the diameter of Earth.
The Moon's gravity helps stabilize Earth's axis
and causes tides that gradually slow Earth's rotation.
Due to tidal locking,
the same side of the Moon always faces Earth.
