Earth, like most other bodies in the Solar System,
[formed 4.5 billion years ago](age_of_Earth) from the gas in the [early Solar System](formation_and_evolution_of_the_Solar_System).
During Earth's first [billion years](billion_years) of [history](history_of_Earth),
the ocean formed, and life subsequently developed within it.
Life spread globally, significantly altering Earth's atmosphere and surface,
leading to the [Great Oxidation Event](Great_Oxidation_Event) two billion years ago.
Humans emerged [300,000 years ago](Human_history) in Africa and
have since spread to every continent except [Antarctica](Antarctica).
Humans rely on Earth's [biosphere](biosphere) and natural resources for survival
but have increasingly impacted the planet's environment.
Humanity's current impact on Earth's climate and biosphere is [unsustainable](sustainability),
threatening the well-being of humans and many other life forms,
and [causing widespread extinctions](Holocene_extinction).
