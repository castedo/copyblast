Earth has a dynamic atmosphere,
which sustains the planet's surface conditions and shields it from most meteoroids and UV light.
Composed primarily of nitrogen and oxygen, the atmosphere also contains water vapor, which contributes to cloud formation that blankets much of the planet.
Water vapor,
along with other greenhouse gases like
carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun's light,
enabling both liquid surface water and water vapor to exist.
This greenhouse effect maintains the Earth's average surface temperature at 14.76&nbsp;°C,
suitable for water to remain liquid at atmospheric pressure.

Variations in the amount of energy captured in different geographic regions—such as the equatorial region receiving more sunlight than the polar regions—drive atmospheric and ocean currents.
These currents contribute to a global climate system characterized by diverse climate regions and a spectrum of weather phenomena, including precipitation.
This dynamic system allows for the cycling of elements like nitrogen through various biogeochemical processes.
