This process maintains the current average surface temperature of 14.76&nbsp;°C, which allows water to remain liquid at atmospheric pressure.
