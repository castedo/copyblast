**Earth** is the third [planet](planet) from the [Sun](Sun) and
the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
Earth is a [water world](ocean_world),
the only one in the [Solar System](Solar_System) with liquid [surface water](surface_water).
Almost all of Earth's water is in its global ocean,
covering [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The remaining 29.2% of Earth's crust is land,
mostly in the form of [continental](continent) [landmasses](landmass) within one [hemisphere](Hemispheres_of_Earth),
known as Earth's [land hemisphere](land_hemisphere).
Most of Earth's land is [humid](humid) and covered by vegetation,
while the [ice sheets](Ice_sheet) at [Earth's polar regions](Earth's_polar_regions) and [deserts](desert) hold more water than
Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
Earth's crust is made up of slowly moving [tectonic plates](plate_tectonics),
which create mountain ranges, [volcanoes](volcano), and earthquakes.
[Earth has a liquid outer core](Earth's_outer_core) that generates a [magnetosphere](magnetosphere),
which deflects most of the harmful [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).
