Antarctica is [governed by about 30 countries](Territorial_claims_in_Antarctica),
all of which are signatories to the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
The treaty
prohibits military activity, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal)
in Antarctica.
The main human activities on the continent include [tourism](Tourism_in_Antarctica), fishing, and research.
During the summer months,
approximately 5,000 people reside at [research stations](Research_stations_in_Antarctica),
a number that decreases to about 1,000 during the winter.
Despite its remoteness,
human activity significantly impacts the continent through
[pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
