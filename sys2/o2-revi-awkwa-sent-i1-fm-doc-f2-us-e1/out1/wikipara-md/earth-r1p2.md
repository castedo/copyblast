Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains its surface conditions and
protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
It is composed primarily of [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also prevalent,
[forming clouds](Cloud#Formation) that envelop much of the planet.
This water vapor, acting as a [greenhouse gas](greenhouse_gas),
along with other greenhouse gases in the atmosphere,
particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
traps
[energy from the Sun's light](Solar_irradiance).
This trapping of energy maintains the Earth's average surface temperature at 14.76&nbsp;°C,
which allows water to remain liquid under atmospheric pressure.
Variations in the amount of energy captured in different geographic regions—such
as the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions)—drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
resulting in a global [climate system](climate_system) with diverse [climate regions](climate_region).
This system gives rise to a variety of weather phenomena, including [precipitation](precipitation),
and enables elements like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through the environment.
