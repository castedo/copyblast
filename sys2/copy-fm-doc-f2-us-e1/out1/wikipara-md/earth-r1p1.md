**Earth** is the third [planet](planet) from the [Sun](Sun) and
the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
This is enabled by Earth being a [water world](ocean_world),
the only one in the [Solar System](Solar_System) sustaining liquid [surface water](surface_water).
Almost all of Earth's water is contained in its global ocean,
covering [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The remaining 29.2% of Earth's crust is land,
most of which is located in the form of [continental](continent) [landmasses](landmass) within one [hemisphere](Hemispheres_of_Earth),
Earth's [land hemisphere](land_hemisphere).
Most of Earth's land is somewhat [humid](humid) and covered by vegetation,
while large [sheets of ice](Ice_sheet) at [Earth's polar](Earth's_polar_regions) [deserts](desert) retain more water than
Earth's [groundwater](groundwater), lakes, rivers and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
Earth's crust consists of slowly moving [tectonic plates](plate_tectonics),
which interact to produce mountain ranges, [volcanoes](volcano), and earthquakes.
[Earth has a liquid outer core](Earth's_outer_core) that generates a [magnetosphere](magnetosphere)
capable of deflecting most of the destructive [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).
