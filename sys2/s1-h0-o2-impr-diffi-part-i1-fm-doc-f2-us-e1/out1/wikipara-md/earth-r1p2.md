The
Earth's atmosphere is dynamic,
sustaining surface conditions and shielding the planet from most meteoroids and harmful UV radiation.
Composed mainly of nitrogen and oxygen,
the atmosphere also contains water vapor, which contributes to cloud formation that blankets much of the Earth.
Water vapor, along with other greenhouse gases like carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun,
maintaining an average surface temperature of 14.76&nbsp;°C,
conducive to liquid water and atmospheric water vapor.
This energy capture varies by
region, with the equatorial area receiving more sunlight than the polar regions,
driving atmospheric and ocean currents.
These currents are part of a global climate system that encompasses diverse climate regions and weather phenomena, such as precipitation, facilitating the cycling of elements like nitrogen within the biosphere.
