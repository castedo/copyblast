Antarctica is, on average, the coldest, driest, and windiest of the continents,
and it has the highest average [elevation](elevation).
It is mainly a [polar desert](polar_desert),
with annual [precipitation](Climate_of_Antarctica#Precipitation) of over 200 mm (8 in) along the coast and far less inland.
About 70% of the world's [freshwater](freshwater) reserves are frozen in Antarctica,
which, if melted, would raise global [sea levels](sea_level) by almost 60 meters (200 ft).
Antarctica holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth),
−89.2 °C (−128.6 °F).
The coastal regions can reach temperatures over 10 °C (50 °F) in the summer.
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) occurs, it is mostly in the form of [lichen](lichen) or [moss](moss).
