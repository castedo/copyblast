Antarctica is, on average, the coldest, driest, and windiest of the continents,
and it has the highest average [elevation](elevation).
It is primarily a [polar desert](polar_desert),
with annual [precipitation](Climate_of_Antarctica#Precipitation) exceeding 200 mm (8 in) along the coast and significantly less inland.
Approximately 70% of the world's [freshwater](freshwater) reserves are encased in Antarctica,
which, if thawed, would cause global [sea levels](sea_level) to rise by nearly 60 metres (200 ft).
Antarctica is the site of the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth),
a frigid &minus;89.2 °C (&minus;128.6 °F).
However, coastal areas can experience temperatures above 10 °C (50 °F) during the summer.
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) is found, it predominantly consists of [lichen](lichen) and [moss](moss).
