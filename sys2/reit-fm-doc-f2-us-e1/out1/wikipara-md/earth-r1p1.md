**Earth** is the third [planet](planet) from the [Sun](Sun) and
the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
Due to Earth being a [water world](ocean_world),
it is the sole body in the [Solar System](Solar_System) with liquid [surface water](surface_water).
The vast majority of Earth's water is in its global ocean,
which covers [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The other 29.2% of Earth's crust is terrestrial,
primarily composed of [continental](continent) [landmasses](landmass) concentrated in one [hemisphere](Hemispheres_of_Earth),
known as Earth's [land hemisphere](land_hemisphere).
Most terrestrial areas on Earth are relatively [humid](humid) and feature abundant vegetation,
while the [sheets of ice](Ice_sheet) at [Earth's polar](Earth's_polar_regions) [deserts](desert) hold more water than
the combined total of Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere).
The Earth's crust is made up of slowly shifting [tectonic plates](plate_tectonics),
whose interactions result in the formation of mountain ranges, [volcanoes](volcano), and earthquakes.
[Earth has a liquid outer core](Earth's_outer_core) that creates a [magnetosphere](magnetosphere)
which is effective at repelling most of the harmful [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).
