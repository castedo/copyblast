Antarctica is [governed by about 30 countries](Territorial_claims_in_Antarctica),
all of which are signatories to the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
The treaty
stipulates that military operations, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal)
are strictly forbidden on the continent.
The primary human activities in Antarctica include [tourism](Tourism_in_Antarctica), fishing,
and scientific research.
During the austral summer, approximately 5,000 individuals inhabit various [research stations](Research_stations_in_Antarctica),
with this number decreasing to about 1,000 during the winter season.
Despite its isolation,
Antarctica is significantly impacted by human actions through
[pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
