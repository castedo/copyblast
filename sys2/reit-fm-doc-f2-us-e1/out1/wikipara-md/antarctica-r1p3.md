The [ice shelves of Antarctica](Antarctic_ice_sheet) are believed to have been first sighted in 1820
during [a Russian expedition](First_Russian_Antarctic_Expedition)
led by [Fabian Gottlieb von Bellingshausen](Fabian_Gottlieb_von_Bellingshausen) and [Mikhail Lazarev](Mikhail_Lazarev).
Subsequent decades witnessed additional [exploration](List_of_Antarctic_expeditions)
by French, American, and British teams.
The first verified landing on the continent was made by a Norwegian group in 1895.
The early 20th century saw several ventures
into Antarctica's interior.
[British explorers](Nimrod_Expedition) were the pioneers in reaching the [magnetic South Pole](South_magnetic_pole) in 1909,
and the [geographic South Pole](Geographic_south_pole) was first conquered in 1911 by [Norwegian explorers](Amundsen's_South_Pole_expedition).
