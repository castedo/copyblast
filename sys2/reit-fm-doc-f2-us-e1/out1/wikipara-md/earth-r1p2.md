The Earth possesses [a dynamic atmosphere](Atmosphere_of_Earth) that not only maintains the
surface conditions but also shields
it from the majority of [meteoroids](meteoroid) and [UV-light upon entry](ozone_layer).
Its atmosphere is predominantly composed of [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is abundantly found within the atmosphere,
[forming clouds](Cloud#Formation) that envelop much of the planet.
This water vapor functions as a [greenhouse gas](greenhouse_gas),
and in conjunction with other greenhouse gases,
especially [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it traps
[energy from the Sun's light](Solar_irradiance).
This trapping of solar energy ensures the persistence of both liquid surface water and atmospheric water vapor, maintaining an average surface temperature of 14.76&nbsp;°C,
which is conducive to water remaining liquid at atmospheric pressure.
The uneven distribution of trapped energy across different regions,
such as the [equatorial region](equatorial_region) receiving more sunlight compared to the [polar regions](polar_regions),
propels [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
which in turn generate a global [climate system](climate_system) with diverse [climate regions](climate_region).
This system gives rise to various weather events, including [precipitation](precipitation),
and facilitates the cycling of elements like [nitrogen](nitrogen_cycle) through [biogeochemical processes](Biogeochemical_cycle).
