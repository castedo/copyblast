Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of approximately 40,000 km.
It is the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Among the four [rocky planets](terrestrial_planet),
Earth is the largest and most massive.
Our planet is situated about eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit),
taking roughly 365.25 days to complete one revolution.

[Earth rotates](Earth's_rotation) on its axis in just under a day
(approximately 23 hours and 56 minutes).
[Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted
with respect to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.

Our planet is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon),
which circles Earth at a distance of 384,400 km (1.28 light seconds) and has
a diameter about a quarter that of Earth.
The Moon's gravitational pull is instrumental in stabilizing Earth's axis
and also generates [tides](tide) that [gradually slow Earth's rotation](Tidal_acceleration).
Due to [tidal locking](tidal_locking),
we always see the same side of the Moon facing Earth.
