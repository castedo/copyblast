The [ice shelves of Antarctica](Antarctic_ice_sheet) were probably first seen in 1820
during [a Russian expedition](First_Russian_Antarctic_Expedition)
led by [Fabian Gottlieb von Bellingshausen](Fabian_Gottlieb_von_Bellingshausen) and [Mikhail Lazarev](Mikhail_Lazarev).
The decades that followed saw further [exploration](List_of_Antarctic_expeditions)
by French, American, and British expeditions.
The first confirmed landing was by a Norwegian team in 1895.
In the early 20th century,
there were a few expeditions into the interior of the continent.
[British explorers](Nimrod_Expedition) were the first to reach the [magnetic South Pole](South_magnetic_pole) in 1909,
and the [geographic South Pole](Geographic_south_pole) was first reached in 1911 by [Norwegian explorers](Amundsen's_South_Pole_expedition).
