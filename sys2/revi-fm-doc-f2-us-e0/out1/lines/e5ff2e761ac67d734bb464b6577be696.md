Where [vegetation](Antarctic_flora) occurs, it is primarily in the form of [lichens](lichen) and [mosses](moss).
