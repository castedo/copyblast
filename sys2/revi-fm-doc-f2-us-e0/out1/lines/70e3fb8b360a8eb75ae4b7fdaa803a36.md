The Earth's crust is composed of slowly moving [tectonic plates](plate_tectonics) that interact to create mountain ranges, [volcanoes](volcano), and earthquakes.
