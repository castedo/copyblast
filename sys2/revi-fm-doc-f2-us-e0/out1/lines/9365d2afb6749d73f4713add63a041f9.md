**Earth** is the third [planet](planet) from the [Sun](Sun) and the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
