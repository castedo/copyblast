Approximately 70% of the world's [freshwater](freshwater) reserves are locked in the ice of Antarctica, which, if melted, could raise global [sea levels](sea_level) by nearly 60 meters (200 feet).
