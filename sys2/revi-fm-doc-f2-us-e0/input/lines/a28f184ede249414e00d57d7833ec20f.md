Antarctica holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth), at −89.2 °C (−128.6 °F).
