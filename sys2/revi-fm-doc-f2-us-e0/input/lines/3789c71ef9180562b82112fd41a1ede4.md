Native species of animals in Antarctica include [mites](Wildlife_of_Antarctica), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
