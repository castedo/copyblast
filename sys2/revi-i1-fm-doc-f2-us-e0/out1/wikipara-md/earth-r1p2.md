The Earth has a dynamic atmosphere,
which sustains the planet's surface conditions
and protects it from most meteoroids and UV light upon entry.
Its composition is primarily nitrogen and oxygen.
Water vapor is also widely present in the atmosphere,
forming clouds that cover most of the planet.
The water vapor acts as a greenhouse gas and,
together with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO<sub>2</sub>),
creates the conditions for both liquid surface water and
water vapor to persist by capturing energy from the Sun's light.
This process maintains the current average surface temperature of 14.76&nbsp;°C,
at which water is liquid under atmospheric pressure.

Differences in the amount of captured energy between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions, drive atmospheric and ocean currents.
This produces a global climate system with different climate regions and a range of weather phenomena, such as precipitation.
These conditions allow components like nitrogen to cycle through the environment.
