The [ice shelves of Antarctica](Antarctic_ice_sheet) were likely first observed in 1820
during [a Russian expedition](First_Russian_Antarctic_Expedition)
led by [Fabian Gottlieb von Bellingshausen](Fabian_Gottlieb_von_Bellingshausen) and [Mikhail Lazarev](Mikhail_Lazarev).
Subsequent decades witnessed additional [exploration](List_of_Antarctic_expeditions)
by French, American, and British teams.
The first confirmed landing on the continent was achieved by a Norwegian group in 1895.
Early 20th-century
expeditions ventured further into Antarctica's interior.
[British explorers](Nimrod_Expedition) were the first to reach the [magnetic South Pole](South_magnetic_pole) in 1909,
and the [geographic South Pole](Geographic_south_pole) was conquered in 1911 by [Norwegian explorers](Amundsen's_South_Pole_expedition).
