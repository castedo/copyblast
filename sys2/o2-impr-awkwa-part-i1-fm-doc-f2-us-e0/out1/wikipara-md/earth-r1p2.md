Earth has a dynamic atmosphere,
which sustains Earth's surface conditions and
protects it from most meteoroids and UV light.
It
is composed primarily
of nitrogen and oxygen.
Water vapor is also prevalent, forming clouds that cover much of the planet.
The water vapor,
along with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun's light.
This process maintains the current average surface temperature of 14.76&nbsp;°C,
allowing for the persistence of liquid surface water and water vapor under atmospheric pressure.

Differences in the amount of energy captured between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions, drive atmospheric and ocean currents.
This results in a global climate system with various climate regions and a range of weather phenomena, including precipitation.
These conditions enable elements like nitrogen to cycle through different forms and processes.
