**Antarctica** is [Earth](Earth)'s southernmost continent and its least populated.
Situated almost entirely south of the [Antarctic Circle](Antarctic_Circle)
and surrounded by the [Southern Ocean](Southern_Ocean)
(also known as the [Antarctic Ocean](Antarctic_Ocean)),
it is home to the geographic [South Pole](South_Pole).
Antarctica ranks as the fifth-largest continent,
being about 40% larger than [Europe](Europe),
and spans an area of 14,200,000 km² (5,500,000 sq mi).
The majority of Antarctica is covered by the [Antarctic ice sheet](Antarctic_ice_sheet),
which has an average thickness of 1.9 km (1.2 mi).
