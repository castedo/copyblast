Earth possesses [a dynamic atmosphere](Atmosphere_of_Earth) that not only
sustains the planet's surface conditions but also shields
it from the majority of [meteoroids](meteoroid) and [harmful UV radiation](ozone_layer).
Its composition is predominantly [nitrogen](nitrogen) and [oxygen](oxygen),
 with [water vapor](Water_vapor) being a common constituent,
[forming clouds](Cloud#Formation) that envelop much of the Earth.
Water vapor functions as a [greenhouse gas](greenhouse_gas),
and in concert with other greenhouse gases in the atmosphere,
especially [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it traps
[energy from the Sun's light](Solar_irradiance).
This effect sustains the planet's average surface temperature at 14.76&nbsp;°C,
a level suitable for liquid water to exist under atmospheric pressure.

The uneven distribution of trapped energy across different geographic areas—such as the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions)—propels
[atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
giving rise to a complex global [climate system](climate_system) with diverse [climate regions](climate_region).
This system generates a variety of weather events, including [precipitation](precipitation),
and facilitates the cycling of elements like [nitrogen](nitrogen_cycle) through various [biogeochemical processes](Biogeochemical_cycle).
