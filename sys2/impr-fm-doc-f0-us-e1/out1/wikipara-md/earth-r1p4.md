Earth, like most celestial bodies in the Solar System,
[formed 4.5 billion years ago](age_of_Earth) from the gas and dust of the [early Solar System](formation_and_evolution_of_the_Solar_System).
During Earth's initial [billion years](billion_years) of existence,
oceans emerged, and subsequently, [life began](Abiogenesis) within these vast waters.
Life proliferated across the planet, significantly altering Earth's atmosphere and surface,
which led to the [Great Oxidation Event](Great_Oxidation_Event) approximately two billion years ago.

Humans appeared [300,000 years ago](Human_history) in Africa and
have since colonized every continent except [Antarctica](Antarctica).
Our species relies on Earth's [biosphere](biosphere) and its natural resources for survival,
yet we have [profoundly affected the planet's environment](Human_impact_on_the_environment).
The current rate at which humanity is impacting Earth's climate and biosphere is [unsustainable](sustainability),
posing a threat to the well-being of humans and countless other life forms,
and is [leading to widespread extinctions](Holocene_extinction).
