Earth has a dynamic atmosphere,
which sustains Earth's surface conditions and
protects it from most meteoroids and UV light.
It
is composed primarily of nitrogen and oxygen.
Water vapor is widely present in the atmosphere,
forming clouds that cover most of the planet.
The water vapor acts as a greenhouse gas and,
together with other greenhouse gases in the atmosphere,
particularly carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun's light.
This process maintains the current average surface temperature of 14.76&nbsp;°C,
allowing water to exist as a liquid under atmospheric pressure.
Variations in the amount of trapped energy between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions,
drive atmospheric and ocean currents.
This results in a global climate system with different climate regions and a range of weather phenomena, such as precipitation, which enables elements like nitrogen to cycle through the environment.
