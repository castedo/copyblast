Earth, like most other bodies in the Solar System,
formed 4.5 billion years ago from the gas present in
the early Solar System.
During Earth's first billion years,
the ocean emerged, and life began to develop within it.
Life proliferated across the globe, significantly altering Earth's atmosphere and surface,
which led to the Great Oxidation Event two billion years ago.
Humans appeared 300,000 years ago in Africa and
have since spread to every continent except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival,
yet their increasing impact on the planet's environment is profound.
The current human influence on Earth's climate and biosphere is unsustainable,
jeopardizing the well-being of humans and numerous other life forms,
and is causing widespread extinctions.
