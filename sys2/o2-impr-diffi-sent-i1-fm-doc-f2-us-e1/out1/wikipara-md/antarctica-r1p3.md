The [ice shelves of Antarctica](Antarctic_ice_sheet) were likely first observed in 1820
during [a Russian expedition](First_Russian_Antarctic_Expedition)
led by [Fabian Gottlieb von Bellingshausen](Fabian_Gottlieb_von_Bellingshausen) and [Mikhail Lazarev](Mikhail_Lazarev).
Subsequent decades witnessed further [exploration](List_of_Antarctic_expeditions)
by French, American, and British teams.
The first confirmed landing on
the continent occurred in 1895 by a Norwegian group.
In the early 20th century,
several expeditions ventured into Antarctica's interior.
[British explorers](Nimrod_Expedition) were the first to reach the [magnetic South Pole](South_magnetic_pole) in 1909,
and [Norwegian explorers](Amundsen's_South_Pole_expedition) first arrived at the [geographic South Pole](Geographic_south_pole) in 1911.
