Earth is shaped into an ellipsoid and has a circumference of approximately 40,000 km.
It is the densest planet in the Solar System.
Among the four rocky planets,
Earth is the largest and most massive.
It is situated about eight light-minutes from the Sun and orbits it,
taking a year (roughly 365.25 days) to complete one orbit.
Earth rotates on its axis in just under a day
(approximately 23 hours and 56 minutes).
The axis of Earth's rotation is tilted relative
to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.

Earth is orbited by one permanent natural satellite, the Moon, which is about 384,400 km (1.28 light seconds) away and
is roughly one-quarter the diameter of Earth.
The Moon's gravitational influence helps stabilize Earth's axial tilt
and causes tides that gradually slow Earth's rotation.
Due to tidal locking,
the same hemisphere of the Moon is always facing Earth.
