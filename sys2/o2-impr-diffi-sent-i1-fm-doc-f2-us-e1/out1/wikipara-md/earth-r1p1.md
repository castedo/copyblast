**Earth** is the third [planet](planet) from the [Sun](Sun) and
the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
Earth is distinguished as a [water world](ocean_world),
the sole body in the [Solar System](Solar_System) with liquid [surface water](surface_water).
The vast majority of Earth's water resides in its global ocean,
which covers [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The remaining 29.2% of Earth's crust constitutes land,
predominantly found in the form of [continental](continent) [landmasses](landmass) within one [hemisphere](Hemispheres_of_Earth),
known as Earth's [land hemisphere](land_hemisphere).
Most of Earth's land is relatively [humid](humid) and blanketed with vegetation,
while the extensive [sheets of ice](Ice_sheet) at [Earth's polar](Earth's_polar_regions) [deserts](desert) hold more water than
Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
Earth's crust is composed of slowly shifting [tectonic plates](plate_tectonics),
whose interactions give rise to mountain ranges, [volcanoes](volcano), and earthquakes.
[Earth has a liquid outer core](Earth's_outer_core) that generates a [magnetosphere](magnetosphere),
which is instrumental in deflecting the majority of harmful [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).
