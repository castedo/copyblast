This process maintains the current average surface temperature of 14.76&nbsp;°C, a temperature at which water remains liquid at atmospheric pressure.
