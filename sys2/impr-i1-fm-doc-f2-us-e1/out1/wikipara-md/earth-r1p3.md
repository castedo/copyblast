Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of approximately 40,000&nbsp;km.
It is the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Among the four [rocky planets](terrestrial_planet),
Earth is the largest and most massive.
Earth is situated about eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit),
taking a year (roughly 365.25 days) to complete one revolution.

[Earth rotates](Earth's_rotation) around its axis in just under a day
(approximately 23 hours and 56 minutes).
[Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted
with respect to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.

Earth is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon),
which circles Earth at a distance of 384,400&nbsp;km (1.28 light seconds) and
is about a quarter of
Earth's diameter.
The Moon's gravitational pull is instrumental in stabilizing Earth's axis
and also causes [tides](tide) that [gradually slow Earth's rotation](Tidal_acceleration).
Due to [tidal locking](tidal_locking),
the same hemisphere of the Moon consistently faces Earth.
