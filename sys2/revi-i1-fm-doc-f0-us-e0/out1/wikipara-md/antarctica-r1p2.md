Antarctica is, on average, the coldest, driest, and windiest continent,
with the highest average [elevation](elevation).
It is primarily a [polar desert](polar_desert),
receiving annual [precipitation](Climate_of_Antarctica#Precipitation) of over 200 mm (8 in) along the coast and much less inland.
Approximately 70% of the world's [freshwater](freshwater) reserves are locked in Antarctica's ice,
which, if melted, would raise global [sea levels](sea_level) by nearly 60 meters (200 ft).
Antarctica also holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth),
at −89.2 °C (−128.6 °F).
However, coastal regions can experience temperatures above 10 °C (50 °F) during the summer.
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) is found, it is predominantly in the form of [lichens](lichen) and [mosses](moss).
