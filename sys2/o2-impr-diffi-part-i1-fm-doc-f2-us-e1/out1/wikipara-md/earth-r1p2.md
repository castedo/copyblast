The Earth has a dynamic atmosphere,
which sustains the planet's surface conditions and protects it from most meteoroids and UV light upon entry.
Its composition is primarily nitrogen and oxygen, with water vapor also widely present.
This water vapor forms clouds that cover much of the planet.
Acting as a greenhouse gas,
water vapor,
along with other greenhouse gases like carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun's light.
This process maintains the Earth's average surface temperature at 14.76&nbsp;°C,
allowing for the persistence of liquid surface water and water vapor under atmospheric pressure.

Differences in the amount of energy captured between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions, drive atmospheric and ocean currents.
These currents produce a global climate system with various climate regions and a range of weather phenomena, including precipitation.
This allows for the cycling of components like nitrogen within the biosphere.
