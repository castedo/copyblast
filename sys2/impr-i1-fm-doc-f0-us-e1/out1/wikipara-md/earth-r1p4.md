Earth, like most celestial bodies in the Solar System,
[formed 4.5 billion years ago](age_of_Earth) from the gas present in the [early Solar System](formation_and_evolution_of_the_Solar_System).
During the first [billion years](billion_years) of [Earth's history](history_of_Earth),
the oceans formed, and subsequently, [life developed](Abiogenesis) within them.
Life proliferated across the globe and began to alter Earth's atmosphere and surface,
leading to the [Great Oxidation Event](Great_Oxidation_Event) approximately two billion years ago.
Humans emerged [300,000 years ago](Human_history) in Africa and
have since dispersed to every continent except [Antarctica](Antarctica).
Humans rely on Earth's [biosphere](biosphere) and its natural resources for survival,
yet they have [increasingly impacted the planet's environment](Human_impact_on_the_environment).
The current human influence on Earth's climate and biosphere is [unsustainable](sustainability),
posing a threat to the well-being of humans and numerous other life forms,
and [causing widespread extinctions](Holocene_extinction).
