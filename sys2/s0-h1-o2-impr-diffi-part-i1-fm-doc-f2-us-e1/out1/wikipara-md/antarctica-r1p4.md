Antarctica is [governed by approximately 30 countries](Territorial_claims_in_Antarctica),
all of which are signatories to the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
Under the treaty's provisions,
military operations, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal)
are all forbidden on the continent.
[Tourism](Tourism_in_Antarctica), fishing, and scientific research are the primary human activities in and around Antarctica.
During the summer,
about 5,000 individuals reside at [research stations](Research_stations_in_Antarctica),
a number that diminishes to approximately 1,000 during the winter months.
Despite its isolation,
human presence significantly impacts the continent through
[pollution](pollution), [ozone layer depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
