Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and
protects it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also widely present,
[forming clouds](Cloud#Formation) that cover much of the planet.
Water vapor acts as a [greenhouse gas](greenhouse_gas),
and together with other greenhouse gases in the atmosphere,
particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it captures
[energy from the Sun's light](Solar_irradiance).
This process maintains the Earth's average surface temperature at 14.76&nbsp;°C,
a temperature at which water remains liquid under atmospheric pressure.

Differences in the amount of energy captured between geographic regions, such as the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions),
drive [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current).
These currents produce a global [climate system](climate_system) with various [climate regions](climate_region)
and a range of weather phenomena, including [precipitation](precipitation).
This allows components like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through the environment.
