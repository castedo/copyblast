Antarctica is [governed by approximately 30 countries](Territorial_claims_in_Antarctica),
all of which are signatories to the 1959 [Antarctic Treaty System](Antarctic_Treaty_System).
Under the treaty's provisions,
military operations, mining, [nuclear explosions](nuclear_explosion), and [nuclear waste disposal](nuclear_waste_disposal)
are all strictly prohibited on the continent.
The primary human activities in and around Antarctica include [tourism](Tourism_in_Antarctica), fishing,
and scientific research.
During the austral summer,
around 5,000 individuals reside at various [research stations](Research_stations_in_Antarctica),
a number that diminishes to about 1,000 during the winter months.
Despite its isolation,
the continent is significantly impacted by human actions through
[pollution](pollution), [ozone depletion](ozone_depletion), and [climate change](Climate_change_in_Antarctica).
