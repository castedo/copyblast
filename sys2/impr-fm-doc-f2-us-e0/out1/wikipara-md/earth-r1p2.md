Earth possesses [a dynamic atmosphere](Atmosphere_of_Earth) that not only
sustains the planet's surface conditions but also shields
it from the majority of [meteoroids](meteoroid) and [UV radiation](ozone_layer).
Its composition is predominantly [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also prevalent,
[forming clouds](Cloud#Formation) that envelop much of the Earth.
This water vapor functions as a [greenhouse gas](greenhouse_gas),
and in conjunction with other greenhouse gases in the atmosphere,
especially [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it traps
[energy from the Sun's light](Solar_irradiance).
This trapping of solar energy maintains the Earth's average surface temperature at 14.76&nbsp;°C,
a level suitable for liquid water to exist under atmospheric pressure.

The uneven distribution of trapped energy across different geographic areas—such as the [equatorial region](equatorial_region) receiving more sunlight compared to the [polar regions](polar_regions)—propels both
[atmospheric](atmospheric_circulation) and [ocean currents](ocean_current).
This movement results in a global [climate system](climate_system) characterized by diverse [climate regions](climate_region)
and a variety of weather events, including [precipitation](precipitation).
Such dynamics enable elements like [nitrogen](nitrogen_cycle) to [cycle](Biogeochemical_cycle) through different environmental components.
