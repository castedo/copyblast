Most of Earth's land is relatively [humid](humid) and covered by vegetation.
In contrast, the extensive [ice sheets](Ice_sheet) at the [Earth's polar regions](Earth's_polar_regions) contain more water than the planet's [groundwater](groundwater) reserves, lakes, rivers, and [atmospheric water vapor](Water_vapor#In_Earth's_atmosphere) combined.
