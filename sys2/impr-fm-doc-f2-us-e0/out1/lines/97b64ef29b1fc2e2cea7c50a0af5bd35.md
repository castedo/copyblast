Life has spread globally and has significantly altered Earth's atmosphere and surface, leading to the [Great Oxidation Event](Great_Oxidation_Event) approximately two billion years ago.
