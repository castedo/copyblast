Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of about 40,000&nbsp;km.
