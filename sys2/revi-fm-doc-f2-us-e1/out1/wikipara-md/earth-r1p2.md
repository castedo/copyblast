The Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and shields
it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also prevalent in the atmosphere,
[forming clouds](Cloud#Formation) that envelop much of the Earth.
This water vapor functions as a [greenhouse gas](greenhouse_gas), and,
in conjunction with other greenhouse gases,
particularly [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it traps
[energy from the Sun's light](Solar_irradiance).
This process helps maintain the Earth's average surface temperature at 14.76&nbsp;°C,
a level suitable for liquid water to exist under atmospheric pressure.

Variations in the amount of energy captured in different geographic areas—such as the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions)—propel
[atmospheric](atmospheric_circulation) and [ocean currents](ocean_current).
These currents contribute to a global [climate system](climate_system) characterized by diverse [climate regions](climate_region)
and a spectrum of weather events, including [precipitation](precipitation).
Such dynamics allow for the cycling of elements like [nitrogen](nitrogen_cycle) through various [biogeochemical processes](Biogeochemical_cycle).
