**Earth** is the third [planet](planet) from the [Sun](Sun) and
the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
Earth is distinguished as a [water world](ocean_world),
the sole body in the [Solar System](Solar_System) with liquid [surface water](surface_water).
The vast majority of Earth's water is in its global ocean,
which spans [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The remaining 29.2% of Earth's crust constitutes land,
predominantly found as [continental](continent) [landmasses](landmass) within one [hemisphere](Hemispheres_of_Earth),
known as Earth's [land hemisphere](land_hemisphere).
Most of Earth's land features a [humid](humid) climate and is blanketed by vegetation,
while the extensive [sheets of ice](Ice_sheet) at [Earth's polar](Earth's_polar_regions) regions and [deserts](desert) hold more water than
Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
The crust of Earth is composed of slowly shifting [tectonic plates](plate_tectonics),
whose interactions create mountain ranges, [volcanoes](volcano), and earthquakes.
[Earth has a liquid outer core](Earth's_outer_core) that produces a [magnetosphere](magnetosphere),
which is crucial in deflecting the majority of harmful [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).
