The first confirmed landing on Antarctica was achieved by a Norwegian team in 1895.
Subsequent early 20th-century
expeditions ventured into the continent's interior.
British explorers were the first to reach the magnetic South Pole in 1909,
and the geographic South Pole was first reached in 1911 by a Norwegian team.
