The Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that
sustains the planet's surface conditions and shields
it from most [meteoroids](meteoroid) and [UV light upon entry](ozone_layer).
Its composition is primarily [nitrogen](nitrogen) and [oxygen](oxygen).
[Water vapor](Water_vapor) is also prevalent in the atmosphere,
[forming clouds](Cloud#Formation) that envelop much of the planet.
Water vapor functions as a [greenhouse gas](greenhouse_gas), and,
in conjunction with other greenhouse gases in the atmosphere,
especially [carbon dioxide](carbon_dioxide) (CO<sub>2</sub>),
it traps
[energy from the Sun's light](Solar_irradiance).
This trapping of solar energy maintains the Earth's average surface temperature at 14.76&nbsp;°C,
a level suitable for liquid water to exist under atmospheric pressure.

The uneven distribution of trapped energy across different geographic areas, such as the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions),
propels [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current).
This movement results in a global [climate system](climate_system) with diverse [climate regions](climate_region)
and a variety of weather phenomena, including [precipitation](precipitation).
Such dynamics allow for the cycling of elements like [nitrogen](nitrogen_cycle) through various [biogeochemical processes](Biogeochemical_cycle).
