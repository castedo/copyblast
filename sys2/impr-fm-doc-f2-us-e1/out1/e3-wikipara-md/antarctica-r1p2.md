Antarctica is, on average, the coldest, driest, and windiest continent,
boasting the highest average [elevation](elevation).
It is primarily a [polar desert](polar_desert),
with annual [precipitation](Climate_of_Antarctica#Precipitation) exceeding 200 mm (8 in) along the coast and far less inland.
Approximately 70% of the world's [freshwater](freshwater) reserves are contained within Antarctica's ice,
which, if melted, would cause global [sea levels](sea_level) to rise by nearly 60 meters (200 ft).
Antarctica also holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth),
at −89.2 °C (−128.6 °F).
Nonetheless, coastal regions can experience temperatures above 10 °C (50 °F) during the summer months.
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) exists, it is primarily composed of [lichens](lichen) and [mosses](moss).
