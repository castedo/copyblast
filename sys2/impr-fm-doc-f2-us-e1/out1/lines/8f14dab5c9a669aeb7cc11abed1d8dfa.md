The Earth is approximately eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit), taking roughly a year (about 365.25 days) to complete a single revolution.
