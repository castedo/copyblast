The Moon's gravity contributes to the stabilization of Earth's axis and also generates [tides](tide), which [gradually decelerate Earth's rotation](Tidal_acceleration).
