During the summer months, nearly 5,000 individuals reside at [research stations](Research_stations_in_Antarctica), a figure that drops to about 1,000 during the winter.
