During the summer months, nearly 5,000 individuals reside at [research stations](Research_stations_in_Antarctica), a number that decreases to approximately 1,000 during the winter.
