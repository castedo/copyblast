Nearly all of Earth's water is contained within its vast oceans, which cover [70.8%](water_distribution_on_Earth) of the [Earth's surface](Earth's_crust).
