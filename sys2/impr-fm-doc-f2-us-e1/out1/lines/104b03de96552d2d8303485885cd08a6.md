Most of Earth's land is relatively [humid](humid) and covered by vegetation.
In contrast, the expansive [ice sheets](Ice_sheet) at the [Earth's polar regions](Earth's_polar_regions), which are classified as [deserts](desert), hold more water than all of Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
