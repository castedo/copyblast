Life has proliferated across the globe, profoundly transforming Earth's atmosphere and surface.
This led to the [Great Oxidation Event](Great_Oxidation_Event) roughly two billion years ago.
