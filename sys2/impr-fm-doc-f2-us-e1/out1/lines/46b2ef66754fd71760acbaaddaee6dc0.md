Where [vegetation](Antarctic_flora) occurs, it primarily appears as [lichen](lichen) or [moss](moss).
