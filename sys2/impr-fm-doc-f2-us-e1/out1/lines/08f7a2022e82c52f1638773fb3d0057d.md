Most of Earth's land is relatively [humid](humid) and covered with vegetation.
In contrast, the vast [ice sheets](Ice_sheet) at the [Earth's polar regions](Earth's_polar_regions), classified as [deserts](desert), contain more water than all of Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
