Earth has [a dynamic atmosphere](Atmosphere_of_Earth) that sustains the planet's surface conditions and protects it from most [meteoroids](meteoroid) and [ultraviolet radiation](ozone_layer).
