This is possible because Earth is a [water world](ocean_world), the only one in the [Solar System](Solar_System) known to sustain liquid [surface water](surface_water).
