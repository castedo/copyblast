**Earth** is the third [planet](planet) from the [Sun](Sun) and
the only [astronomical object](astronomical_object) known to [harbor](Planetary_habitability) life.
As a [water world](ocean_world),
Earth is the only body in the [Solar System](Solar_System) with liquid [surface water](surface_water).
The vast majority of Earth's water is contained in its global ocean,
which covers [70.8%](water_distribution_on_Earth) of [Earth's crust](Earth's_crust).
The remaining 29.2% consists of continents,
primarily clustered within one [hemisphere](Hemispheres_of_Earth)—the
[land hemisphere](land_hemisphere).

Most of Earth's land surface is relatively [humid](humid) and covered with vegetation.
In contrast,
the [ice sheets](Ice_sheet) in [Earth's polar regions](Earth's_polar_regions) and [deserts](desert) contain more water than
Earth's [groundwater](groundwater), lakes, rivers, and [atmospheric water](Water_vapor#In_Earth's_atmosphere) combined.
The planet's crust is composed of slowly moving [tectonic plates](plate_tectonics),
whose interactions result in the formation of mountain ranges, [volcanoes](volcano), and earthquakes.

[Earth has a liquid outer core](Earth's_outer_core) that generates a [magnetosphere](magnetosphere),
 a protective field capable of deflecting most of the harmful [solar winds](solar_wind) and [cosmic radiation](cosmic_radiation).
