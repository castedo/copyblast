The Earth boasts a [dynamic atmosphere](Atmosphere_of_Earth) that not only sustains
the planet's surface conditions but also shields
it from the majority of [meteoroids](meteoroid) and [UV radiation](ozone_layer).
Its primary constituents are [nitrogen](nitrogen) and [oxygen](oxygen),
 with [water vapor](Water_vapor) also playing a pivotal role.
Water vapor is essential for the formation of [clouds](Cloud#Formation), which frequently enshroud the planet.
As a [greenhouse gas](greenhouse_gas), water vapor,
along with other greenhouse gases—most notably
[carbon dioxide](carbon_dioxide) (CO<sub>2</sub>)—helps to trap [solar energy](Solar_irradiance).
This greenhouse effect maintains the Earth's average surface temperature at a hospitable 14.76&nbsp;°C,
allowing water to exist as a liquid under standard atmospheric pressure.

The distribution of solar energy varies across different geographic regions, with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions).
This disparity in energy absorption is the primary driver of both [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
which are integral to the global [climate system](climate_system).
This system features a variety of [climate regions](climate_region)
and a spectrum of weather phenomena, including [precipitation](precipitation).
Such a dynamic system facilitates the cycling of elements like [nitrogen](nitrogen_cycle) through a series of [biogeochemical processes](Biogeochemical_cycle).
