**Antarctica** is [Earth](Earth)'s southernmost continent and its least populated.
Situated almost entirely south of the [Antarctic Circle](Antarctic_Circle)
and surrounded by the [Southern Ocean](Southern_Ocean)
(also known as the [Antarctic Ocean](Antarctic_Ocean)),
it hosts the geographic [South Pole](South_Pole).
As the fifth-largest continent,
Antarctica is about 40% larger than [Europe](Europe),
spanning an area of 14,200,000 km² (5,500,000 sq mi).
The vast majority of the continent is covered by the [Antarctic ice sheet](Antarctic_ice_sheet),
which has an average thickness of 1.9 km (1.2 mi).
