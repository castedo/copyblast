The Earth boasts a [dynamic atmosphere](Atmosphere_of_Earth) that not only sustains
the planet's surface conditions but also protects
it from the majority of [meteoroids](meteoroid) and [UV radiation](ozone_layer).
Its primary components are [nitrogen](nitrogen) and [oxygen](oxygen),
 with [water vapor](Water_vapor) also playing a significant role.
Water vapor is crucial for the formation of [clouds](Cloud#Formation), which frequently envelop the planet.
As a [greenhouse gas](greenhouse_gas), water vapor,
along with other greenhouse gases—most notably
[carbon dioxide](carbon_dioxide) (CO<sub>2</sub>)—helps to retain [solar energy](Solar_irradiance).
This greenhouse effect maintains the Earth's average surface temperature at a comfortable 14.76&nbsp;°C,
enabling water to exist as a liquid under standard atmospheric pressure.

The distribution of solar energy is uneven across different geographic regions, with the [equatorial region](equatorial_region) receiving more sunlight than the [polar regions](polar_regions).
This variation in energy absorption is the driving force behind both [atmospheric](atmospheric_circulation) and [ocean currents](ocean_current),
which are essential components of the global [climate system](climate_system).
This system is characterized by diverse [climate regions](climate_region)
and a range of weather phenomena, including [precipitation](precipitation).
Such a dynamic system enables the cycling of elements like [nitrogen](nitrogen_cycle) through a series of [biogeochemical processes](Biogeochemical_cycle).
