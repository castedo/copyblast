The Earth is shaped into an [ellipsoid](Earth_ellipsoid) due to [hydrostatic equilibrium](Hydrostatic_equilibrium) and has [a circumference](Earth's_circumference) of approximately 40,000 kilometers.
