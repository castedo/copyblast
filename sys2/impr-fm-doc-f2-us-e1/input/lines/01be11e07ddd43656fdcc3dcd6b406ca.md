Earth is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite)—the [Moon](Moon).
The Moon orbits Earth at an average distance of 384,400 km (1.28 light seconds) and has a diameter that is approximately one-quarter that of Earth.
