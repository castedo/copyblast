Approximately 70% of the world's [freshwater](freshwater) reserves are contained within the ice of Antarctica.
Should this ice melt, it would result in a global [sea level](sea_level) rise of approximately 60 meters (200 feet).
