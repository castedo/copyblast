This is made possible because Earth is a [water world](ocean_world), the only one in the [Solar System](Solar_System) that sustains liquid [surface water](surface_water).
