The Earth's crust is composed of slowly shifting [tectonic plates](plate_tectonics) that interact to form mountain ranges, [volcanoes](volcano), and earthquakes.
