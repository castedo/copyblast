The Earth's crust is composed of slowly shifting [tectonic plates](plate_tectonics) that interact, forming mountain ranges, [volcanoes](volcano), and causing earthquakes.
