Where [vegetation](Antarctic_flora) occurs, it primarily manifests as [lichen](lichen) or [moss](moss).
