The first confirmed landing on Antarctica was by a Norwegian team in 1895.
In the early 20th century,
several expeditions ventured into the interior of the continent.
British explorers were the first to reach the magnetic South Pole in 1909,
and the geographic South Pole was first reached in 1911 by Norwegian explorers.
