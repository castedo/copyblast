The
Earth's atmosphere is dynamic,
sustaining the planet's surface conditions
and shielding it from
the majority of meteoroids and harmful UV light.
Composed mainly of nitrogen and oxygen,
the atmosphere also contains water vapor, which contributes to cloud formation that blankets much of the Earth.
Water vapor, along with other greenhouse gases like carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun, a process essential for maintaining the planet's
average surface temperature of 14.76&nbsp;°C.
This temperature allows water to exist as a liquid at atmospheric pressure.
The uneven distribution of trapped solar energy across different geographic regions,
such as the equatorial region receiving more sunlight than the polar regions,
drives atmospheric and ocean currents.
These currents are integral to a global climate system characterized by diverse climate regions and a variety of weather phenomena, including precipitation.
This dynamic system enables the cycling of elements like nitrogen through different environmental pathways.
