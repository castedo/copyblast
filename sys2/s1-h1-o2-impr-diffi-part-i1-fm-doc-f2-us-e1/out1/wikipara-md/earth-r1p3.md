Earth is
the densest planet in the Solar System and the largest and most massive of the four rocky planets.
It is approximately eight light-minutes from the Sun and orbits it,
taking about 365.25 days to complete one revolution.
Earth rotates around its axis in roughly
23 hours and 56 minutes.
The axial tilt of Earth is responsible for the changing seasons.
Our planet is orbited by one natural satellite, the Moon, which is about a quarter of Earth's diameter
and orbits at a distance of 384,400 km.
The Moon's gravitational influence stabilizes Earth's axial tilt
and causes tides, which gradually slow Earth's rotation.
Due to tidal locking,
we always see the same side of the Moon from Earth.
