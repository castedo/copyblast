During Earth's first billion years,
the ocean formed and life began within it.
Life then spread globally, altering Earth's atmosphere and surface,
leading to the Great Oxidation Event two billion years ago.
Humans emerged 300,000 years ago in Africa and
have since inhabited every continent except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival
but have increasingly affected the planet's environment.
The current human impact on Earth's climate and biosphere is unsustainable,
threatening the well-being of humans and many other life forms,
and causing widespread extinctions.
