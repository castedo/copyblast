Earth, like most other bodies in the Solar System,
formed 4.5 billion years ago from gas in the early Solar System.
During the first billion years of Earth's history,
the ocean formed and life developed within it.
Life spread globally and has been altering Earth's atmosphere and surface,
leading to the Great Oxidation Event two billion years ago.
Humans emerged 300,000 years ago in Africa and
have spread across every continent on Earth, except Antarctica.
Humans rely on Earth's biosphere and natural resources for survival,
but have increasingly impacted the planet's environment.
Humanity's current impact on Earth's climate and biosphere is unsustainable,
threatening the well-being of humans and many other forms of life,
and causing widespread extinctions.
