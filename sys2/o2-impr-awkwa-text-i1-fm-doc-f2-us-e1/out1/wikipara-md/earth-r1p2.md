Earth has a dynamic atmosphere,
which sustains the planet's surface conditions and protects it from most meteoroids and UV light.
Its composition is primarily nitrogen and oxygen, with water vapor also widely present.
This water vapor forms clouds that cover much of the planet.
Acting as a greenhouse gas,
along with other gases like
carbon dioxide (CO<sub>2</sub>),
water
vapor helps capture energy from the Sun's light.
This process maintains the Earth's average surface temperature at 14.76&nbsp;°C,
allowing for liquid water to exist under atmospheric pressure.
The variation in energy capture between geographic regions,
such as the equatorial region receiving more sunlight than the polar regions,
drives atmospheric and ocean currents.
These currents contribute to a global climate system with diverse climate regions and a range of weather phenomena, including precipitation.
This dynamic system enables elements like nitrogen to participate in various biogeochemical cycles.
