The text provided does not contain any confusing sentences that require improvement.
It is clear and informative,
detailing the history of the exploration of Antarctica's ice shelves and significant achievements in reaching the South Pole.
