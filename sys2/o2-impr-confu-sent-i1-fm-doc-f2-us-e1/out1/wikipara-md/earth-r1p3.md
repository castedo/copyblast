Earth is [rounded](Hydrostatic_equilibrium) into [an ellipsoid](Earth_ellipsoid) with [a circumference](Earth's_circumference) of about 40,000&nbsp;km and
is the [densest planet in the Solar System](list_of_Solar_System_objects_by_size).
Among the four [rocky planets](terrestrial_planet),
Earth is the largest and most massive.
It is situated approximately eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit),
taking about 365.25 days to complete one revolution.

[Earth rotates](Earth's_rotation) around its axis in just under a day
(approximately 23 hours and 56 minutes).
The [Earth's axis of rotation](#Axial_tilt_and_seasons) is tilted
with respect to the perpendicular of its orbital plane around the Sun,
which results in the changing seasons.

Earth is orbited by one [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite), the [Moon](Moon),
which is at a distance of 384,400&nbsp;km (1.28 light seconds) and has
a diameter about a quarter that of Earth.
The Moon's gravitational pull helps stabilize Earth's axial tilt
and causes [tides](tide), which [gradually slow Earth's rotation](Tidal_acceleration).
Due to [tidal locking](tidal_locking),
the same side of the Moon always faces Earth.
