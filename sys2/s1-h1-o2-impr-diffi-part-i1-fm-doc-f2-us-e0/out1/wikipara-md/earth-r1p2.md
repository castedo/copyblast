The
Earth's atmosphere is dynamic,
sustaining the planet's surface conditions
and shielding it from
the majority of meteoroids and harmful UV radiation.
Composed mainly of nitrogen and oxygen,
the atmosphere also contains water vapor, which contributes to cloud formation that blankets much of the Earth.
Water vapor, along with other greenhouse gases like carbon dioxide (CO<sub>2</sub>),
traps
energy from the Sun,
a process essential for maintaining the average surface temperature of 14.76&nbsp;°C,
which allows water to remain liquid at atmospheric pressure.

Geographic disparities in energy absorption, such as the equatorial region receiving more sunlight than the polar regions,
drive atmospheric and ocean currents.
This results in a global climate system characterized by various climate regions and a spectrum of weather events,
including precipitation.
These conditions enable essential elements like nitrogen to participate in biogeochemical cycles.
