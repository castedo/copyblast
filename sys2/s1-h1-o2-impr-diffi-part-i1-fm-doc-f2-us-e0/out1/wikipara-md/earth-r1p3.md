Earth is
the [densest planet in the Solar System](list_of_Solar_System_objects_by_size) and
the largest and most massive of the four [rocky planets](terrestrial_planet).
It
is located about eight [light-minutes](light-minute) from the Sun and [orbits it](Earth's_orbit) in
a year, which is approximately 365.25 days.
Earth [rotates](Earth's_rotation) on its axis in just under a day—about 23 hours and 56 minutes.
The [axis of Earth's rotation](#Axial_tilt_and_seasons) is tilted relative
to the perpendicular of its orbital plane around the Sun,
giving rise to the seasons.

Earth's only [permanent](claimed_moons_of_Earth) [natural satellite](natural_satellite) is the [Moon](Moon),
which orbits at a distance of 384,400&nbsp;km (1.28 light seconds) and has
a diameter about a quarter that of Earth.
The Moon's gravitational pull is instrumental in stabilizing Earth's axial tilt
and in generating [tides](tide), which [gradually slow Earth's rotation](Tidal_acceleration).
Due to [tidal locking](tidal_locking),
the same hemisphere of the Moon consistently faces Earth.
