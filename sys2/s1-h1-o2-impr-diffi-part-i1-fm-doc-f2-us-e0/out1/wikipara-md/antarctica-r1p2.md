Antarctica is, on average, the coldest, driest, and windiest continent,
and it has the highest average [elevation](elevation).
It is primarily a [polar desert](polar_desert),
with annual [precipitation](Climate_of_Antarctica#Precipitation) exceeding 200 mm (8 in) along the coast and significantly less inland.
About 70% of the world's [freshwater](freshwater) reserves are locked in Antarctica's ice,
which, if melted, would cause global [sea levels](sea_level) to rise by nearly 60 meters (200 ft).
Antarctica holds the record for the [lowest measured temperature on Earth](Lowest_temperature_recorded_on_Earth),
&minus;89.2 °C (&minus;128.6 °F).
The coastal regions can experience temperatures above 10 °C (50 °F) during the summer.
Native [species of animals](Wildlife_of_Antarctica) include [mites](mite), [nematodes](nematode), [penguins](penguin), [seals](Pinniped), and [tardigrades](tardigrade).
Where [vegetation](Antarctic_flora) is found, it predominantly consists of [lichen](lichen) and [moss](moss).
